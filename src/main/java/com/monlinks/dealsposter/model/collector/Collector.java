package com.monlinks.dealsposter.model.collector;

import com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.DealFetchersEnum;
import com.monlinks.dealsposter.model.CategoriesEnums;
import com.monlinks.dealsposter.model.MerchantLocation;
import com.monlinks.dealsposter.model.common.AudienceLocationEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Document(collection = "Collectors_10-05-2017")
public class Collector {

	@Id
	private String id;
	
	private String name;
	private boolean enabled = true;
	
	private CollectType collectType;
	private String collectValue;//category name or merchant list (with ,)
	private MerchantLocation merchantLocation;
	
	private ArrayList<AudienceLocationEnum>  targetAudienceLocations;
	
	private DealFetchersEnum dealsFetcher;
	private CategoriesEnums categoryToBeSavedWith;
	
	public Collector(String name, boolean enabled, CollectType collectType, String collectValue,
			MerchantLocation merchantLocation, ArrayList<AudienceLocationEnum> targetAudienceLocations,
			DealFetchersEnum dealsFetcher, CategoriesEnums categoryToBeSavedWith) {
		super();
		this.name = name;
		this.enabled = enabled;
		this.collectType = collectType;
		this.collectValue = collectValue;
		this.merchantLocation = merchantLocation;
		this.targetAudienceLocations = targetAudienceLocations;
		this.dealsFetcher = dealsFetcher;
		this.categoryToBeSavedWith = categoryToBeSavedWith;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public CollectType getCollectType() {
		return collectType;
	}

	public void setCollectType(CollectType collectType) {
		this.collectType = collectType;
	}

	public String getCollectValue() {
		return collectValue;
	}

	public void setCollectValue(String collectValue) {
		this.collectValue = collectValue;
	}

	public MerchantLocation getMerchantLocation() {
		return merchantLocation;
	}

	public void setMerchantLocation(MerchantLocation merchantLocation) {
		this.merchantLocation = merchantLocation;
	}

	public ArrayList<AudienceLocationEnum> getTargetAudienceLocations() {
		return targetAudienceLocations;
	}

	public void setTargetAudienceLocations(ArrayList<AudienceLocationEnum> targetAudienceLocations) {
		this.targetAudienceLocations = targetAudienceLocations;
	}

	public DealFetchersEnum getDealsFetcher() {
		return dealsFetcher;
	}

	public void setDealsFetcher(DealFetchersEnum dealsFetcher) {
		this.dealsFetcher = dealsFetcher;
	}

	public CategoriesEnums getCategoryToBeSavedWith() {
		return categoryToBeSavedWith;
	}

	public void setCategoryToBeSavedWith(CategoriesEnums categoryToBeSavedWith) {
		this.categoryToBeSavedWith = categoryToBeSavedWith;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Collector [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", enabled=");
		builder.append(enabled);
		builder.append(", collectType=");
		builder.append(collectType);
		builder.append(", collectValue=");
		builder.append(collectValue);
		builder.append(", merchantLocation=");
		builder.append(merchantLocation);
		builder.append(", targetAudienceLocations=");
		builder.append(targetAudienceLocations);
		builder.append(", dealsFetcher=");
		builder.append(dealsFetcher);
		builder.append(", categoryToBeSavedWith=");
		builder.append(categoryToBeSavedWith);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
	
}
