package com.monlinks.dealsposter.model.publisher;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;

@Document(collection = "Publishers_12-07-2017")
public class Publisher {

	@Id
	private String id;
	private String name;
	private boolean enabled = true;
	private String[] publishScedulers;// cron strings //WHEN
	private PostingUser postingUser;//WHO
	private PublisherJob[] publisherJobs;//WHERE

	public Publisher(String name, String[] publishScedulers, PostingUser postingUser,PublisherJob[] publisherJobs,boolean enabled) {
		super();
		
		this.name = name;
		this.publishScedulers = publishScedulers;
		this.postingUser = postingUser;
		this.setPublisherJobs(publisherJobs);
		this.setEnabled(enabled);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getPublishScedulers() {
		return publishScedulers;
	}

	public void setPublishScedulers(String[] publishScedulers) {
		this.publishScedulers = publishScedulers;
	}

	public PostingUser getPostingUser() {
		return postingUser;
	}

	public void setPostingUser(PostingUser postingUser) {
		this.postingUser = postingUser;
	}

	public PublisherJob[] getPublisherJobs() {
		return publisherJobs;
	}

	public void setPublisherJobs(PublisherJob[] publisherJobs) {
		this.publisherJobs = publisherJobs;
	}

	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Publisher [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", enabled=");
		builder.append(enabled);
		builder.append(", publishScedulers=");
		builder.append(Arrays.toString(publishScedulers));
		builder.append(", postingUser=");
		builder.append(postingUser);
		builder.append(", publisherJobs=");
		builder.append(Arrays.toString(publisherJobs));
		builder.append("]");
		return builder.toString();
	}

	

	

}
