package com.monlinks.dealsposter.model.publisher;

public class PublisherJob {
	private String publisherName;
	private DealsSelector dealsSelector;
	private PostingDestination postingDestination;


	public PublisherJob(String publisherName, DealsSelector dealsSelector, PostingDestination postingDestination) {
		this.publisherName = publisherName;
		this.dealsSelector = dealsSelector;
		this.postingDestination = postingDestination;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public DealsSelector getDealsSelector() {
		return dealsSelector;
	}

	public void setDealsSelector(DealsSelector dealsSelector) {
		this.dealsSelector = dealsSelector;
	}

	public PostingDestination getPostingDestination() {
		return postingDestination;
	}

	public void setPostingDestination(PostingDestination postingDestination) {
		this.postingDestination = postingDestination;
	}

	@Override
	public String toString() {
		return "PublisherJob{" +
				"publisherName='" + publisherName + '\'' +
				", dealsSelector=" + dealsSelector +
				", postingDestination=" + postingDestination +
				'}';
	}
}
