package com.monlinks.dealsposter.model.publisher;

import com.monlinks.dealsposter.model.MerchantLocation;
import com.monlinks.dealsposter.model.collector.CollectType;
import com.monlinks.dealsposter.model.common.AudienceLocationEnum;

import java.util.Arrays;

public class DealsSelector {
	private CollectType selectorType;
	//private CategoriesEnums dealsCategory;
	private String selectString;
	private MerchantLocation[] merchantLocations;
	private int dealsBulkSize;
	private AudienceLocationEnum audienceLocation;

	public DealsSelector(CollectType selectorType, String selectString, MerchantLocation[] merchantLocations,  AudienceLocationEnum audienceLocation,int dealsBulkSize) {
		this.selectorType = selectorType;
		this.selectString = selectString;
		this.merchantLocations = merchantLocations;
		this.dealsBulkSize = dealsBulkSize;
		this.audienceLocation = audienceLocation;
	}

	public CollectType getSelectorType() {
		return selectorType;
	}

	public void setSelectorType(CollectType selectorType) {
		this.selectorType = selectorType;
	}

	public String getSelectString() {
		return selectString;
	}

	public void setSelectString(String selectString) {
		this.selectString = selectString;
	}

	public MerchantLocation[] getMerchantLocations() {
		return merchantLocations;
	}

	public void setMerchantLocations(MerchantLocation[] merchantLocations) {
		this.merchantLocations = merchantLocations;
	}

	public int getDealsBulkSize() {
		return dealsBulkSize;
	}

	public void setDealsBulkSize(int dealsBulkSize) {
		this.dealsBulkSize = dealsBulkSize;
	}

	public AudienceLocationEnum getAudienceLocation() {
		return audienceLocation;
	}

	public void setAudienceLocation(AudienceLocationEnum audienceLocation) {
		this.audienceLocation = audienceLocation;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("DealsSelector{");
		sb.append("selectorType=").append(selectorType);
		sb.append(", selectString='").append(selectString).append('\'');
		sb.append(", merchantLocations=").append(Arrays.toString(merchantLocations));
		sb.append(", dealsBulkSize=").append(dealsBulkSize);
		sb.append(", audienceLocation=").append(audienceLocation);
		sb.append('}');
		return sb.toString();
	}
}
