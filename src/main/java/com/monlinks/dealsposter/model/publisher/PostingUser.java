package com.monlinks.dealsposter.model.publisher;

public class PostingUser {
	private String publisherName;
	private String postingUserId;
	private String userAuthKey;
	private String affUser;// for future use

	public PostingUser(String publisherName, String postingUserId, String userAuthKey, String affUser ) {
		super();
		this.setPublisherName(publisherName);
		this.postingUserId = postingUserId;
		this.userAuthKey = userAuthKey;
		this.affUser = affUser;
	}

	public String getPostingUserId() {
		return postingUserId;
	}

	public void setPostingUserId(String postingUserId) {
		this.postingUserId = postingUserId;
	}

	public String getUserAuthKey() {
		return userAuthKey;
	}

	public void setUserAuthKey(String userAuthKey) {
		this.userAuthKey = userAuthKey;
	}

	public String getAffUser() {
		return affUser;
	}

	public void setAffUser(String affUser) {
		this.affUser = affUser;
	}

	

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PostingUser [publisherName=");
		builder.append(publisherName);
		builder.append(", postingUserId=");
		builder.append(postingUserId);
		builder.append(", userAuthKey=");
		builder.append(userAuthKey);
		builder.append(", affUser=");
		builder.append(affUser);
		builder.append("]");
		return builder.toString();
	}
	
	

}
