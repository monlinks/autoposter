package com.monlinks.dealsposter.model.publisher;

public class PostingDestination {
	
	private SocialNetworksEnum socialNetwork; // Social Network name
	private DestinationTypeEnum destinationType;// group/page
	private String destinationId;
	private String destinationName;
	private PostLangEnum postLang;
	
	public enum SocialNetworksEnum {
		FACEBOOK

	}
	
	public enum PostLangEnum {
		HEB, ENG
	}

	public enum DestinationTypeEnum {
		GROUP, PAGE
	}

	

	public PostingDestination(SocialNetworksEnum socialNetwork, DestinationTypeEnum destinationType, String destinationId,String destinationName, PostLangEnum postLang) {
		super();
		this.setSocialNetwork(socialNetwork);
		this.destinationType = destinationType;
		this.destinationId = destinationId;
		this.destinationName = destinationName;
		this.postLang = postLang;
	}



	public DestinationTypeEnum getDestinationType() {
		return destinationType;
	}

	public void setDestinationType(DestinationTypeEnum destinationType) {
		this.destinationType = destinationType;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public PostLangEnum getPostLang() {
		return postLang;
	}

	public void setPostLang(PostLangEnum postLang) {
		this.postLang = postLang;
	}



	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}



	public SocialNetworksEnum getSocialNetwork() {
		return socialNetwork;
	}



	public void setSocialNetwork(SocialNetworksEnum socialNetwork) {
		this.socialNetwork = socialNetwork;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PostingDestination [socialNetwork=");
		builder.append(socialNetwork);
		builder.append(", destinationType=");
		builder.append(destinationType);
		builder.append(", destinationId=");
		builder.append(destinationId);
		builder.append(", destinationName=");
		builder.append(destinationName);
		builder.append(", postLang=");
		builder.append(postLang);
		builder.append("]");
		return builder.toString();
	}



	
}
