package com.monlinks.dealsposter.model.publisher;

import com.monlinks.dealsposter.model.publisher.PostingDestination.SocialNetworksEnum;

public class PublishAction {
	
	private SocialNetworksEnum socialNetwork;
	private String postDate;
	private String postId;
	private ActionResultEnum actionResult;
	private String errorDescription;
	private String destinationId;
	private String destinationName;
	private String publishedAffLink;
	private String publishedShortAffLink;

	
	public PublishAction(SocialNetworksEnum socialNetwork, String postDate, String postId, ActionResultEnum actionResult,
						  String errorDescription, String destinationId, String destinationName,String publishedAffLink,String publishedShortAffLink) {
		super();
		this.socialNetwork = socialNetwork;
		this.postDate = postDate;
		this.actionResult = actionResult;
		this.errorDescription = errorDescription;
		this.destinationId = destinationId;
		this.setDestinationName(destinationName);
		this.postId = postId;
		this.publishedAffLink = publishedAffLink;
		this.publishedShortAffLink = publishedShortAffLink;
	}

	public SocialNetworksEnum getSocialNetwork() {
		return socialNetwork;
	}

	public void setSocialNetwork(SocialNetworksEnum socialNetwork) {
		this.socialNetwork = socialNetwork;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public ActionResultEnum getActionResult() {
		return actionResult;
	}

	public void setActionResult(ActionResultEnum actionResult) {
		this.actionResult = actionResult;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public String getPublishedAffLink() {
		return publishedAffLink;
	}

	public void setPublishedAffLink(String publishedAffLink) {
		this.publishedAffLink = publishedAffLink;
	}

	public String getPublishedShortAffLink() {
		return publishedShortAffLink;
	}

	public void setPublishedShortAffLink(String publishedShortAffLink) {
		this.publishedShortAffLink = publishedShortAffLink;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("PublishAction{");
		sb.append("socialNetwork=").append(socialNetwork);
		sb.append(", postDate='").append(postDate).append('\'');
		sb.append(", postId='").append(postId).append('\'');
		sb.append(", actionResult=").append(actionResult);
		sb.append(", errorDescription='").append(errorDescription).append('\'');
		sb.append(", destinationId='").append(destinationId).append('\'');
		sb.append(", destinationName='").append(destinationName).append('\'');
		sb.append(", publishedAffLink='").append(publishedAffLink).append('\'');
		sb.append(", publishedShortAffLink='").append(publishedShortAffLink).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
