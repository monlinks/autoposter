package com.monlinks.dealsposter;

import com.monlinks.dealsposter.dao.CollectorsRepository;
import com.monlinks.dealsposter.dao.DealsRepository;
import com.monlinks.dealsposter.dao.PublishersRepository;
import com.monlinks.dealsposter.dealscleaner.DealsCleanerJobExecutor;
import com.monlinks.dealsposter.dealscollectorservice.DealsCollectorJobExecutor;
import com.monlinks.dealsposter.dealsposterservice.DealsPublisherJobExecutor;
import com.monlinks.dealsposter.model.collector.Collector;
import com.monlinks.dealsposter.model.publisher.Publisher;
import com.monlinks.dealsposter.utils.DateTimeHelper;
import com.monlinks.dealsposter.utils.EntitiesBuilder;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.net.URL;
import java.net.URLClassLoader;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@SpringBootApplication
// @EnableScheduling
public class Application implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	@Autowired
	private DealsRepository dealsRepository;
	@Autowired
	private PublishersRepository publishersRepository;
	@Autowired
	private CollectorsRepository collectorsRepository;

	@Value("${collectors.run.enable}")
	private boolean collectorsRunEnable;

	@Value("${publishers.run.enable}")
	private boolean publishersRunEnable;

	@Value("${collector.schedule.cron}")
	private String collectorScheduleCronStr;

	@Value("${publishDeals.schedule.cron.meadweek}")
	private String publishScheduleCronStrMeadweek;

	@Value("${publishDeals.schedule.cron.weekend}")
	private String publishScheduleCronStrWeekend;

	@Value("${fashion.merchants.list}")
	private String fashionMerchantsList;

	@Value("${travel.merchants.list}")
	private String travelMerchantsList;

	@Value("${collectors.run.now.only}")
	private boolean collectorsRunNowOnly;

	@Value("${publishers.run.now.only}")
	private boolean publishersRunNowOnly;

	@Autowired
	Environment environment;

	SchedulerFactory schfa = new StdSchedulerFactory();
	Scheduler sch = schfa.getScheduler();

	public Application() throws SchedulerException {
	}

	// TODO: add resources REST: Get unpublished delas, get Error deals, Delete
	// unpublished , delete errors , run collect now, run publish now,
	// get all posted deals (with facebook post id), get post Id info (clicks ,
	// likes ...)

	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		log.debug("spring.config.location=" + System.getProperty("spring.config.location"));
		// log.info("server.address="+System.getProperty("local.server.address"));
		String port = environment.getProperty("local.server.port");
		log.info("randomServerPort=" + port);

		System.setProperty("file.encoding", "UTF-8");

		printClassPath();

		log.info("**************** START APPLICATION שלום ****************");

		// testNewLogic();

		if(collectorsRunEnable) {
			runCollectors();
		}else{
			log.warn("Collectors are disabled!!!");
		}


		if(publishersRunEnable) {
			runPublishers();
		}else{
			log.warn("Publishers are disabled!!!");
		}

		sch.start();
//
//		// testPostLogic();
//		// testGetRandomUrl();
//		// testGoogleShortener();
//		// testMongoSort();

	}

	private void runCollectors() {
		//After one run the collectors are in DB.
		List<Collector> collectorsToSave = EntitiesBuilder.buildCollectors(fashionMerchantsList,travelMerchantsList);

		//save collectors if needed
		for (Collector collector :collectorsToSave) {
			if(collectorsRepository.findByName(collector.getName()).isEmpty()){
				collectorsRepository.save(collector);
			}
		}

		List<Collector> collectors = collectorsRepository.findByEnabled(true);//get only enabled publishers from DB
		log.info("Collectors="+ collectors);
		if(collectors!=null && !collectors.isEmpty()){
			if(collectorsRunNowOnly) {
				runNowCollectDealsJob(collectors);
			}else {
				scheduelCollectDealsJob3(collectors);
			}
		}
		else{
			log.error("No enabled collectors were found. Will not collect any deals.");
		}

	}

	private void runPublishers() {
		String randomImageFashionStr = "models-woman";
		String randomImageTravelStr = "vacation";

		Publisher monlinksPublisher = EntitiesBuilder.buildPublisher();

		if (publishersRepository.findByName(monlinksPublisher.getName()).isEmpty()) {
			publishersRepository.save(monlinksPublisher);
		}

		Publisher ushopsPublisher = EntitiesBuilder.buildPublisherForUshops();

		if (publishersRepository.findByName(ushopsPublisher.getName()).isEmpty()) {
			publishersRepository.save(ushopsPublisher);
		}

		List<Publisher> publishers = publishersRepository.findByEnabled(true);//get only enabled publishers from DB
		if(publishers!=null && !publishers.isEmpty()){
			for (Publisher publisher : publishers) {
				if(publishersRunNowOnly) {
					runNowCleanDealsJob();
					runNowPublishDealsJob(publisher);
				}else {
					scheduelCleanDealsJob(publisher);
					scheduelPublishDealsJob3(publisher);
				}
			}
		}
		else{
			log.error("No enabled publishers were found. Will not publish any deals.");
		}

	}



	private void scheduelCollectDealsJob3(List<Collector> collectors) {
		log.debug("collectorScheduleCronStr="+collectorScheduleCronStr);
		try {

			Trigger collectSchedual = buildTrigger("collectSchedual", collectorScheduleCronStr);
			JobDetail jobdetail1 = buildJobDetailsForCollectors(collectors);
			sch.scheduleJob(jobdetail1, collectSchedual);

		} catch (SchedulerException | ParseException e) {
			log.error(e.getMessage(), e);
		}
	}


	private void scheduelPublishDealsJob3(Publisher publisher) {
		try {

			Trigger meadWeekTrigger = buildTrigger("meadWeek", publisher.getPublishScedulers()[0]);
			Trigger weekEndTrigger = buildTrigger("weekEnd", publisher.getPublishScedulers()[1]);

			JobDetail jobdetail1 = buildJobDetailsForPublishers(publisher);
			JobDetail jobdetail2 = buildJobDetailsForPublishers(publisher);

			sch.scheduleJob(jobdetail1, meadWeekTrigger);
			sch.scheduleJob(jobdetail2, weekEndTrigger);

		} catch (SchedulerException | ParseException e) {
			log.error(e.getMessage(), e);
		}

	}

	private void scheduelCleanDealsJob(Publisher publisher) {
		try {

			Trigger meadWeekTrigger = buildTrigger("meadWeek", publisher.getPublishScedulers()[0]);
			Trigger weekEndTrigger = buildTrigger("weekEnd", publisher.getPublishScedulers()[1]);

			JobDetail cleanerJob1 = buildJobDetailsForCleaner();
			JobDetail cleanerJob2 = buildJobDetailsForCleaner();

			sch.scheduleJob(cleanerJob1, meadWeekTrigger);
			sch.scheduleJob(cleanerJob2, weekEndTrigger);

		} catch (SchedulerException | ParseException e) {
			log.error(e.getMessage(), e);
		}

	}


	private void runNowPublishDealsJob(Publisher publisher) {
		try {
			Trigger Nowtrigger = TriggerBuilder.newTrigger().startNow().build();
			JobDetail jobdetail = buildJobDetailsForPublishers(publisher);
			sch.scheduleJob(jobdetail, Nowtrigger);

		} catch (SchedulerException e) {
			log.error(e.getMessage(), e);
		}
	}

	private void runNowCleanDealsJob() {
		try {
			Trigger Nowtrigger = TriggerBuilder.newTrigger().startNow().build();

			//run cleaner
			JobDetail cleanerJob1 = buildJobDetailsForCleaner();
			sch.scheduleJob(cleanerJob1, Nowtrigger);

		} catch (SchedulerException e) {
			log.error(e.getMessage(), e);
		}
	}

	public void runNowCollectDealsJob(List<Collector> collectors) {
		try {
			Trigger Nowtrigger = TriggerBuilder.newTrigger().startNow().build();
			JobDetail jobdetail = buildJobDetailsForCollectors(collectors);
			sch.scheduleJob(jobdetail, Nowtrigger);
		} catch (SchedulerException e) {
			log.error(e.getMessage(), e);
		}
	}


	private JobDetail buildJobDetailsForPublishers(Publisher publisher) {
		JobDetail jobdetail = JobBuilder.newJob(DealsPublisherJobExecutor.class).build();
		jobdetail.getJobDataMap().put("publisherId", publisher.getId());
		jobdetail.getJobDataMap().put("dealsRepository", dealsRepository);
		jobdetail.getJobDataMap().put("publishersRepository", publishersRepository);

		return jobdetail;
	}

	private JobDetail buildJobDetailsForCleaner() {
		JobDetail jobdetail = JobBuilder.newJob(DealsCleanerJobExecutor.class).build();
		jobdetail.getJobDataMap().put("dealsRepository", dealsRepository);
		return jobdetail;
	}

	private JobDetail buildJobDetailsForCollectors(List<Collector> collectors) {
		JobDetail jobdetail = JobBuilder.newJob(DealsCollectorJobExecutor.class).build();
		jobdetail.getJobDataMap().put("collectors", collectors);
		jobdetail.getJobDataMap().put("dealsRepository", dealsRepository);
		jobdetail.getJobDataMap().put("collectorsRepository", collectorsRepository);

		return jobdetail;
	}



	public Trigger buildTrigger(String trigerName, String cronPattern) throws ParseException {
		log.info("cronPattern=" + cronPattern);
		CronExpression exp = new CronExpression(cronPattern);
		Date nextRun = exp.getNextValidTimeAfter(new Date());
		log.info(trigerName+" nextRun=" + nextRun);
		log.info(trigerName+" nextRun ISRAEL time=" + DateTimeHelper.calcIsraelTime(nextRun));

		CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(cronPattern);

		Trigger trigger = TriggerBuilder.newTrigger().startNow().withSchedule(cronScheduleBuilder).build();

		return trigger;
	}

	private void printClassPath() {

		// Get the System Classloader
		ClassLoader sysClassLoader = ClassLoader.getSystemClassLoader();

		// Get the URLs
		URL[] urls = ((URLClassLoader) sysClassLoader).getURLs();

		log.debug("ClassPath:");
		for (int i = 0; i < urls.length; i++) {
			log.debug(urls[i].getFile());
		}

	}

}