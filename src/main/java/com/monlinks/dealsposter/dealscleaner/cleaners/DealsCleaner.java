package com.monlinks.dealsposter.dealscleaner.cleaners;

import com.monlinks.dealsposter.dao.DealsRepository;

public interface DealsCleaner {

	public void clean(DealsRepository dealsRepository);
		

}
