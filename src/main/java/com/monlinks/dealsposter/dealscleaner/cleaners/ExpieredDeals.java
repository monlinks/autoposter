package com.monlinks.dealsposter.dealscleaner.cleaners;

import com.monlinks.dealsposter.activity.ActivityAgregator;
import com.monlinks.dealsposter.activity.CleanerActivity;
import com.monlinks.dealsposter.dao.DealsRepository;
import com.monlinks.dealsposter.dealscleaner.SkipReason;
import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.utils.DateFormatConvertor;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ExpieredDeals implements DealsCleaner {
	private static Logger log = LogManager.getLogger(ExpieredDeals.class.getName());

	@Override
	public void clean(DealsRepository dealsRepository) {

		try {

			SimpleDateFormat dateInputFormat = new SimpleDateFormat("ddd MMM dd hh:mm:ss zzz yyyy");
			SimpleDateFormat dateOutputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");// should
			// be
			// "2017-04-13
			// 23:59:59",
			DateFormatConvertor dateFormatConvertor = new DateFormatConvertor(dateInputFormat, dateOutputFormat);

			String dateTime = dateFormatConvertor.currentDateInOutputFormat();

			log.debug("Find deals expiers before this formated date time:" + dateTime);
			List<Deal> dealsExpiersBefore = dealsRepository.findDealsExpiersBefore(dateTime);

			log.debug("Found number of expiered deals:" + dealsExpiersBefore.size());
			for (Deal deal : dealsExpiersBefore) {
				deal.setSkipReason(SkipReason.EXPIERED);
			}

			if (dealsExpiersBefore.size() > 0) {
				dealsRepository.save(dealsExpiersBefore);
			}

			ActivityAgregator.INSTANCE.addActivity(new CleanerActivity("ExpieredDeals",(new Date()).toString(),dateTime,dealsExpiersBefore.size()));


		} catch (Exception e) {
			log.error("Failed to clean expiered deals, before date:" + new Date().toString(), e);
		}

	}
}