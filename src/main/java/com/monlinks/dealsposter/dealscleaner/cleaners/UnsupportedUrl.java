package com.monlinks.dealsposter.dealscleaner.cleaners;

import com.monlinks.dealsposter.dao.DealsRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class UnsupportedUrl implements DealsCleaner {
	private static Logger log = LogManager.getLogger(UnsupportedUrl.class.getName());

    private static String[] unsupportedDomains = {"scripts.affilired.com"};
    
	@Override
	public void clean(DealsRepository dealsRepository) {
		for (String domain : unsupportedDomains) {
			log.debug("Find all deals with this unsupported domain:"+ domain);
			//TODO: implement this
		}
		
	}

}
