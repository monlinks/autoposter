package com.monlinks.dealsposter.dealscleaner;

import com.monlinks.dealsposter.dao.DealsRepository;
import com.monlinks.dealsposter.dealscleaner.cleaners.DealsCleaner;
import com.monlinks.dealsposter.dealscleaner.cleaners.ExpieredDeals;
import com.monlinks.dealsposter.dealscleaner.cleaners.UnsupportedUrl;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;


public class DealsCleanerJobExecutor implements Job {


	private DealsRepository dealsRepository;
	
	private ResourceBundle config = ResourceBundle.getBundle("application",Locale.ROOT);
	private boolean isRunningInTestMode = Boolean.parseBoolean(config.getString("collectDeals.test.mode"));

	private static Logger log = LogManager.getLogger(DealsCleanerJobExecutor.class.getName());

	private static List<DealsCleaner> cleaners = new ArrayList<DealsCleaner>();
	static{
		cleaners.add(new ExpieredDeals());
		cleaners.add(new UnsupportedUrl());
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("############### START DealsCleanerJobExecutor ########################");
		this.dealsRepository = (DealsRepository) context.getJobDetail().getJobDataMap().get("dealsRepository");
		doClean();
		log.info("############### FINISH DealsCleanerJobExecutor ########################");
	}
	
	public void runCleanDirectly(DealsRepository repository) {
		log.info("############### START DealsCleanerJobExecutor (runCollectDirectly) ########################");
		this.dealsRepository = repository;
		doClean();
		log.info("############### FINISH DealsCleanerJobExecutor (runCollectDirectly) ########################");
	
	}
	
	private void doClean(){
		for (DealsCleaner dealsCleaner : cleaners) {
			dealsCleaner.clean(this.dealsRepository);
		}
	}



}
