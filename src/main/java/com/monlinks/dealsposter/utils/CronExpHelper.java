package com.monlinks.dealsposter.utils;

import org.quartz.CronExpression;
import org.quartz.JobExecutionContext;

import java.text.ParseException;
import java.util.Date;

public class CronExpHelper {

	public static Date calcNextRun(String cronExpressionStr) throws ParseException {
			CronExpression exp = new CronExpression(cronExpressionStr);
			Date nextRun = exp.getNextValidTimeAfter(new Date());
			return nextRun;
	}

}
