package com.monlinks.dealsposter.utils;

import java.net.HttpURLConnection;

/**
 * Created by lior on 02/04/2017.
 */
public class AffiliationUrlNotValidException extends Exception {
    String origAffUrl = null;
    String problematicRedirectUrl = null;
    int errorCode = HttpURLConnection.HTTP_INTERNAL_ERROR;

    public AffiliationUrlNotValidException(String message, String origAffUrl, String problematicRedirectUrl, int errorCode) {
        super(message);
        this.origAffUrl = origAffUrl;
        this.problematicRedirectUrl = problematicRedirectUrl;
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return "{message="+ super.getMessage() +
                ", affUrl='" + origAffUrl + '\'' +
                ", problemUrl='" + problematicRedirectUrl + '\'' +
                ", errorCode=" + errorCode +
                '}';
    }

    @Override
    public String toString() {
        return "AffiliationUrlNotValidException{" +
                "origAffUrl='" + origAffUrl + '\'' +
                ", problematicRedirectUrl='" + problematicRedirectUrl + '\'' +
                ", errorCode=" + errorCode +
                '}';
    }
}
