package com.monlinks.dealsposter.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lior on 08/05/2017.
 */
public class DateFormatConvertor {
    private SimpleDateFormat dateInputFormat;
    private SimpleDateFormat dateOutputFormat;

    public DateFormatConvertor(SimpleDateFormat dateInputFormat, SimpleDateFormat dateOutputFormat) {
        this.dateInputFormat = dateInputFormat;
        this.dateOutputFormat = dateOutputFormat;
    }

    public String convert(String inputDateString) throws ParseException {
       // Date date = dateInputFormat.parse(inputDateString.trim());
        Date date = new Date(inputDateString);
        return dateOutputFormat.format(date);
    }
    
    public String currentDateInOutputFormat() throws ParseException {
         Date date = new Date();
         return dateOutputFormat.format(date);
     }
}