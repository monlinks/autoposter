package com.monlinks.dealsposter.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.Map;


public class UrlRelatedHelper {
    private static final Logger log = LoggerFactory.getLogger(UrlRelatedHelper.class);
    private static final String phantomJSUrlTemplate = "https://phantomjscloud.com/api/browser/v2/ak-bdtjm-hctjg-qz46p-yc6ma-c6vw8/?request={url:\"%s\",renderType:\"plainText\",outputAsJson:true}";

    public static String calcFinalAffUrl2(String initialSourceUrl) throws AffiliationUrlNotValidException {
        log.info("START calcFinalAffUrl2 for:"+ initialSourceUrl);
        String finalUrl = null;
        try{
            String urlWithoutUmpersent = initialSourceUrl.replace("&","%26");
            String phantomUrl = String.format(phantomJSUrlTemplate,urlWithoutUmpersent);
            log.debug("phantomUrl="+phantomUrl);
            JsonObject jsonObject = null;
            jsonObject = getJsonObj(phantomUrl);
            JsonElement jsonElement = jsonObject.getAsJsonObject("content").get("url");
            finalUrl = jsonElement.getAsString();
        } catch (IOException e) {
            log.error("Problem get final url from:" + initialSourceUrl, e);
            //will return null
        }
        catch (Exception e) {
            log.error("Problem get final url from:" + initialSourceUrl, e);
            throw new AffiliationUrlNotValidException("Problem get final url. Orig exception msg:" + e.getMessage(), initialSourceUrl, finalUrl, 0);
        }

        log.info("END calcFinalAffUrl2 finalUrl="+ finalUrl);
        return finalUrl;
    }

    protected static JsonObject getJsonObj(String fetchFromUrl) throws MalformedURLException, IOException, JSONException {
        log.debug("fetchFromUrl="+fetchFromUrl);
        URL url = new URL(fetchFromUrl);

        URLConnection connection = url.openConnection();
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(70000);

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));

        String inputLine;
        StringBuilder sb = new StringBuilder();
        while ((inputLine = in.readLine()) != null){
            sb.append(inputLine);
        }
        in.close();

        String jsonString = sb.toString();

        JsonParser jsonParser = new JsonParser();
        JsonObject xmlJSONObj = (JsonObject)jsonParser .parse(jsonString);

        return xmlJSONObj;
    }


@Deprecated
    public static String calcFinalAffUrl(String initialSourceUrl) throws AffiliationUrlNotValidException {

        HttpURLConnection connection;
        String finalUrl = initialSourceUrl;
        boolean gotFinalUrl = false;
        int responseCode = 0;

        try {
            String initialAffDomain = (new URL(initialSourceUrl)).getHost();

            do {
                finalUrl = makeParametersUrlEncoded(finalUrl);
                log.debug("Going to connect to=" + finalUrl);
                URL finalUrlObj = new URL(finalUrl);

                connection = (HttpURLConnection) finalUrlObj.openConnection();
                connection.setInstanceFollowRedirects(false);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");
                connection.connect();
                responseCode = connection.getResponseCode();
                log.debug("responseCode=" + responseCode);

                switch (responseCode) {
                    case HttpURLConnection.HTTP_MOVED_TEMP: {
                        finalUrl = getRedirectUrlFromLocationHeader(connection);
                        break;
                    }
                    case HttpURLConnection.HTTP_MOVED_PERM: {
                        finalUrl = getRedirectUrlFromLocationHeader(connection);
                        break;
                    }
                    case HttpURLConnection.HTTP_OK: {
                        //if we got to the same domain we should find the rediredt script
                        if (finalUrl.contains(initialAffDomain)) {
                            String result = getResponseString(connection);
                            log.debug("result=" + result);
                            finalUrl = getRedirectUrl(result);
                        } else {
                            log.debug("Got to final location:" + finalUrl);
                            gotFinalUrl = true;
                        }
                        break;
                    }

                    case HttpURLConnection.HTTP_BAD_REQUEST: {
                        log.error("Got bad request error for this url:" + finalUrl);
                        gotFinalUrl = true;//to exit the loop
                        //finalUrl = null;
                        throw new AffiliationUrlNotValidException("Got bad request error (400)", initialSourceUrl, finalUrl, responseCode);

                    }
                    case HttpURLConnection.HTTP_FORBIDDEN:{//this probebly say that it is forbiden to access via server (only by browser)
                        log.debug("Got to final location:" + finalUrl);
                        gotFinalUrl = true;
                        break;
                    }
                    default: {
                        log.error("Got response code " + responseCode + ", for URL=" + finalUrl + " ,Orig affUrl=" + initialSourceUrl);
                        gotFinalUrl = true;//to break the loop
                        throw new AffiliationUrlNotValidException("Got unhandled error code", initialSourceUrl, finalUrl, responseCode);
                    }

                }

            } while (!gotFinalUrl);

            connection.disconnect();


        } catch (Exception e) {
            log.error("Problem get final url from:" + initialSourceUrl, e);
            finalUrl = null;
            throw new AffiliationUrlNotValidException("Problem get final url. Orig exception msg:" + e.getMessage(), initialSourceUrl, finalUrl, responseCode);
        }

        log.debug("finalUrl=" + finalUrl);
        return finalUrl;
    }

    private static String makeParametersUrlEncoded(String currUrl) throws MalformedURLException, UnsupportedEncodingException {
        log.debug("currUrl=" + currUrl);
        URL finalUrlObj = new URL(currUrl);

        //get the queryString
		String currentQueryStr = finalUrlObj.getQuery();
		currentQueryStr = currentQueryStr==null?"":currentQueryStr;
		log.debug("currentQueryStr=" + currentQueryStr);

		String currentUrlNoParameters =  currUrl.replace(currentQueryStr, "").trim();
		log.debug("currentUrlNoParameters=" + currentUrlNoParameters);

        Map<String,String> splitQueryMap = splitQuery(finalUrlObj);
        log.debug("splitQueryMap=" + splitQueryMap);

        String encodedQueryString = "";
        for(Map.Entry<String, String> entry : splitQueryMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String decodedValue = URLDecoder.decode(value,"utf-8");
            String encodedValue = URLEncoder.encode(decodedValue,"utf-8");
            encodedQueryString = key.trim()+"="+encodedValue+"&"+encodedQueryString;
        }

        log.debug("encodedQueryString=" + encodedQueryString);
        return currentUrlNoParameters+encodedQueryString;
    }

    private static String getRedirectUrlFromLocationHeader(HttpURLConnection connection) {
        String redirectedUrl = connection.getHeaderField("Location");

        if (null == redirectedUrl) {
            log.error("Problem in redirect url. Got response code 302 but 'Location' is empty");
        }

        if (!redirectedUrl.contains("http")) {
            //solve relative redirect
            String prefixUrl = connection.getURL().getProtocol() + "://" + connection.getURL().getHost();
            redirectedUrl = prefixUrl + redirectedUrl;
        }

        log.debug("redirected url: " + redirectedUrl);
        return redirectedUrl;
    }

    public static Map<String, String> splitQuery(URL url) throws UnsupportedEncodingException {
        final Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        if(url.getQuery()==null) return query_pairs;

        final String[] pairs = url.getQuery().split("&");
        for (String pair : pairs) {
            final int idx = pair.indexOf("=");
            final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
            if (!query_pairs.containsKey(key)) {
                final String value = idx > 0 && pair.length() > idx + 1 ? pair.substring(idx + 1) : "";
                query_pairs.put(key, value);
            }

        }
        return query_pairs;
    }

//	 public static String calcFinalRedirectedUrl(String initialSourceUrl) throws MalformedURLException, IOException{
//	 log.debug("initialSourceUrl="+initialSourceUrl);
//	 String resFinalUrl = initialSourceUrl;
//	 int status = 
//	 
//	 HttpURLConnection connection = (HttpURLConnection) new URL(initialSourceUrl).openConnection();
//    connection.setInstanceFollowRedirects(false);
//    resFinalUrl = connection.getHeaderField("location");
//    
//    
//    log.debug("resFinalUrl="+resFinalUrl);
//	 return resFinalUrl;
//}

    private static String getRedirectUrl(String htmlResult) {
        //String redirectUrl = takeStringBetween(htmlResult,"replace\\(\\'","\\'\\);");
        String redirectUrl = null;
        try {
            redirectUrl = takeStringBetween(htmlResult, "url=http://", "\\\"*/>");//prosperent
        } catch (Exception e) {
            redirectUrl = takeStringBetween(htmlResult, "referLink.href =*'", "';");//yield:  referLink.href = 'https://click.linksynergy.com/fs-bin/click?id=eyzsD2QGsYg&offerid=480627.10047676&type=3&subid=0&u1=v030800009587226a0ef04a1f49f8a5931be8eba70177';
        }
        redirectUrl = redirectUrl.replaceAll("\"", "");//remove the last quoet
        if (!redirectUrl.startsWith("http")) {//could be also https
            redirectUrl = "http://" + redirectUrl;
        }

        log.debug("redirectUrl=" + redirectUrl);
        return redirectUrl;
    }

    private static String takeStringBetween(String fullStr, String leftStr, String rightStr) {
        log.debug("fullStr=" + fullStr);
        log.debug("takeStringBetween:leftStr=" + leftStr + " ,rightStr=" + rightStr);
        String[] split1 = fullStr.split(leftStr);
        String rest = split1[1];
        log.debug("rest=" + rest);
        String[] split2 = rest.split(rightStr);
        String retStr = split2[0];
        log.debug("retStr=" + retStr);
        return retStr;
    }

    public static String getHTML(String urlToRead) throws Exception {
        String result = null;
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setInstanceFollowRedirects(true);

        log.debug("connected url: " + conn.getURL());

        conn.setRequestMethod("GET");
        result = getResponseString(conn);

        return result;
    }

    private static String getResponseString(HttpURLConnection conn) throws IOException {
        StringBuilder result = new StringBuilder();
        if (conn.getResponseCode() == 200) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
        }

        return result.toString();
    }

}