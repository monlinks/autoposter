package com.monlinks.dealsposter.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeHelper {

	public static Date calcIsraelTime(Date nextRun) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(nextRun);
		//openShift Machine (EST -7 hours) 1:00 EST is 8:00 IST
		cal.set(Calendar.HOUR, cal.get(Calendar.HOUR) + 7);
		
		return cal.getTime();
	}
}
