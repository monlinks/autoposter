package com.monlinks.dealsposter.utils;

import com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.DealFetchersEnum;
import com.monlinks.dealsposter.model.CategoriesEnums;
import com.monlinks.dealsposter.model.MerchantLocation;
import com.monlinks.dealsposter.model.collector.CollectType;
import com.monlinks.dealsposter.model.collector.Collector;
import com.monlinks.dealsposter.model.common.AudienceLocationEnum;
import com.monlinks.dealsposter.model.publisher.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lior on 30/05/2017.
 */
public class EntitiesBuilder {
    private static final Logger log = LoggerFactory.getLogger(EntitiesBuilder.class);

    public static Publisher buildPublisherForUshops() {
        String name = "ushops";
        String[] publishScedulers = { "0 7 1,6,13 ? * MON,TUE,WED,THU,SUN *", "0 12 2,5,12 ? * FRI,SAT *" };

        String posterName = "Ushops fashion page";
        String postingUserId = "1973385039563970";
        //with expiers at 30/7/2017
        String userAuthKey = "EAACvVIo6h10BALon3NAPJcLr4pIdnyea1ZC1ecZBmc1cSAp11EBbNj02xTzOKqAZC9lxSF4xBZCAOVmzFhU2s9AAYXdfuDTlTmdZC79MWJ2fXsjKO0mjKDHSwoP4GfSL62WMeQHHZC45ie0qrIC1zGZACHEYA0YC9sZD";
        String affUser = "ushops";
        PostingUser postingUser = new PostingUser(posterName, postingUserId, userAuthKey, affUser);

        MerchantLocation[] merchantLocationsUS = { MerchantLocation.US};


		/*

		  {
            "publisherName": "monlinksPublisher",
            "dealsSelector": {
                "dealsCategory": "TRAVEL_GROUPON",
                "merchantLocations": [
                    "IL"
                ],
                "dealsBulkSize": 1,
                "audienceLocation": "IL"
            },
            "postingDestination": {
                "socialNetwork": "FACEBOOK",
                "destinationType": "GROUP",
                "destinationId": "1807721512802289",
                "destinationName": "-גרופון-מבצעים לאתרי תירות",
                "postLang": "HEB"
            }
        },
		 */



        // FASHION deals
        String domainsList = "ebay.com,6pm.com,walmart.com,nordstrom.com,zulily.com,gap.com,childrensplace.com,jcrew.com,forever21.com,shop.nordstrom.com,shoes.com,payless.com,zappos.com,joesnewbalanceoutlet.com,footlocker.com,nautica.com,columbia.com,ae.com,hanes.com,crazy8.com,carters.com,clarksusa.com,famousfootwear.com";
        DealsSelector fashionDealsSelector = new DealsSelector(CollectType.MERCHANT_LIST, domainsList, merchantLocationsUS,
                AudienceLocationEnum.IL, 5);

        String fashionDestinationId = "1973385039563970";

        PostingDestination fashionPostingDestination = new PostingDestination(PostingDestination.SocialNetworksEnum.FACEBOOK,
                PostingDestination.DestinationTypeEnum.PAGE, fashionDestinationId, "כל-המבצעים-מארהב", PostingDestination.PostLangEnum.HEB);

        PublisherJob fashionPublisherInfo = new PublisherJob(name,fashionDealsSelector, fashionPostingDestination);

        PublisherJob[] publisherJobs = {  fashionPublisherInfo };

        Publisher ushopsFashionPublisher = new Publisher(name, publishScedulers, postingUser, publisherJobs,true);

        log.info("ushopsFashionPublisher=" + ushopsFashionPublisher);

        return ushopsFashionPublisher;
    }

    public static Publisher buildPublisher() {
        String name = "monlinksPublisher";
        String[] publishScedulers = { "0 5 1,6,13 ? * MON,TUE,WED,THU,SUN *", "0 12 2,5,12 ? * FRI,SAT *" };

        String posterName = "Alon Cohen";
        String postingUserId = "100008877286993";// Alon Cohen
        //This Token is valid till: 30/7/2017
        String userAuthKey = "EAACvVIo6h10BALF3yIljoVN5T0eY5ZAkrupFPPGl9epjNhlQOSGFe7Gk4L5ZCIdmZA0K9du0HVSMvOMd6ChNmQfNNmZCBli2Ff1hBZCp9ILGKIDm3HAQfRJYnFiZAMYlEH18u23ZArkY2ZCxZA3zodkZCl4twOtOEBkC4ZD";
        String affUser = "autoPoster";
        PostingUser postingUser = new PostingUser(posterName, postingUserId, userAuthKey, affUser);

        MerchantLocation[] merchantLocationsIsrael = { MerchantLocation.IL, MerchantLocation.US, MerchantLocation.UK };

        // TRAVEL deals HEB
        DealsSelector travelDealsSelector = new DealsSelector(CollectType.CATEGORY,CategoriesEnums.TRAVEL.name(), merchantLocationsIsrael,
                AudienceLocationEnum.IL, 5);

        String travelDestinationId = "1807721512802289";// travel HEB

        PostingDestination travelPostingDestination = new PostingDestination(PostingDestination.SocialNetworksEnum.FACEBOOK,
                PostingDestination.DestinationTypeEnum.GROUP, travelDestinationId, "מבצעים לאתרי תירות", PostingDestination.PostLangEnum.HEB);

        PublisherJob travelPublisherInfo = new PublisherJob(name,travelDealsSelector, travelPostingDestination);

		/*

		  {
            "publisherName": "monlinksPublisher",
            "dealsSelector": {
                "dealsCategory": "TRAVEL_GROUPON",
                "merchantLocations": [
                    "IL"
                ],
                "dealsBulkSize": 1,
                "audienceLocation": "IL"
            },
            "postingDestination": {
                "socialNetwork": "FACEBOOK",
                "destinationType": "GROUP",
                "destinationId": "1807721512802289",
                "destinationName": "-גרופון-מבצעים לאתרי תירות",
                "postLang": "HEB"
            }
        },
		 */

        //Groupon IL travel
        DealsSelector travelGrouponDealsSelector = new DealsSelector(CollectType.CATEGORY,CategoriesEnums.TRAVEL_GROUPON.name(), merchantLocationsIsrael,
                AudienceLocationEnum.IL, 1);
        PublisherJob travelGrouponPublisherInfo = new PublisherJob(name,travelGrouponDealsSelector, travelPostingDestination);

        // TRAVEL deals ENG
        MerchantLocation[] merchantLocationsUS = { MerchantLocation.US };

        DealsSelector travelEngDealsSelector = new DealsSelector(CollectType.CATEGORY,CategoriesEnums.TRAVEL.name(), merchantLocationsUS,
                AudienceLocationEnum.US, 5);

        String travelEngDestinationId = "640687266116417";// travel ENG

        PostingDestination travelEngPostingDestination = new PostingDestination(PostingDestination.SocialNetworksEnum.FACEBOOK,
                PostingDestination.DestinationTypeEnum.GROUP, travelEngDestinationId, "Travel Deals and Coupons", PostingDestination.PostLangEnum.ENG);

        PublisherJob travelEngPublisherInfo = new PublisherJob(name,travelEngDealsSelector, travelEngPostingDestination);

        // FASHION deals
        DealsSelector fashionDealsSelector = new DealsSelector(CollectType.CATEGORY,CategoriesEnums.FASHION.name(), merchantLocationsIsrael,
                AudienceLocationEnum.IL, 5);

        String fashionDestinationId = "1821915298095868";// fashion HEB

        PostingDestination fashionPostingDestination = new PostingDestination(PostingDestination.SocialNetworksEnum.FACEBOOK,
                PostingDestination.DestinationTypeEnum.GROUP, fashionDestinationId, "מבצעים לאתרי אופנה", PostingDestination.PostLangEnum.HEB);

        PublisherJob fashionPublisherInfo = new PublisherJob(name,fashionDealsSelector, fashionPostingDestination);

        PublisherJob[] publisherJobs = { travelPublisherInfo, fashionPublisherInfo, travelEngPublisherInfo,travelGrouponPublisherInfo };

        Publisher monlinksPublisher = new Publisher(name, publishScedulers, postingUser, publisherJobs,true);

        log.info("monlinksPublisher=" + monlinksPublisher);

        return monlinksPublisher;
    }

    public static List<Collector> buildCollectors(String fashionMerchantsList,String travelMerchantsList) {

        List<Collector> collectors = new ArrayList<>();

        //FASHION all
        CategoriesEnums categoryToBeSavedWith = CategoriesEnums.FASHION;
        String collectValue = fashionMerchantsList;
        ArrayList<AudienceLocationEnum> targetAudienceLocationsAll = new ArrayList<AudienceLocationEnum>();
        targetAudienceLocationsAll.add(AudienceLocationEnum.IL);
        targetAudienceLocationsAll.add(AudienceLocationEnum.US);


        Collector fashionUSIcodesCollector = new Collector("fashion_US_Icodes_Collector", true, CollectType.MERCHANT_LIST, collectValue , MerchantLocation.US, targetAudienceLocationsAll , DealFetchersEnum.ICODES, categoryToBeSavedWith );
        collectors.add(fashionUSIcodesCollector);
        Collector fashionUKIcodesCollector = new Collector("fashion_UK_Icodes_Collector", true, CollectType.MERCHANT_LIST, collectValue , MerchantLocation.UK, targetAudienceLocationsAll , DealFetchersEnum.ICODES, categoryToBeSavedWith );
        collectors.add(fashionUKIcodesCollector);
//        Collector fashionUSYieldCollector = new Collector("fashion_US_Yield_Collector", true, CollectType.MERCHANT_LIST, collectValue , MerchantLocation.US, targetAudienceLocationsAll , DealFetchersEnum.YIELD, categoryToBeSavedWith );
//        collectors.add(fashionUSYieldCollector);

//		//Fashion collected by category
//		Collector fashionUSIcodesCollector = new Collector("fashion_US_Icodes_Collector", true, CollectType.CATEGORY, "Apparel" , MerchantLocation.US, targetAudienceLocationsAll , DealFetchersEnum.ICODES, categoryToBeSavedWith );
//		collectors.add(fashionUSIcodesCollector);
//		Collector fashionUKIcodesCollector = new Collector("fashion_UK_Icodes_Collector", true, CollectType.CATEGORY, "Clothing_and_Footwear" , MerchantLocation.UK, targetAudienceLocationsAll , DealFetchersEnum.ICODES, categoryToBeSavedWith );
//		collectors.add(fashionUKIcodesCollector);

        Collector fashionUSSkimlinksCollector = new Collector("fashion_US_Skim_Collector", true, CollectType.CATEGORY, "168" , MerchantLocation.US, targetAudienceLocationsAll , DealFetchersEnum.SKIMLINKS, categoryToBeSavedWith );
        collectors.add(fashionUSSkimlinksCollector);

        //TRAVEL others
        categoryToBeSavedWith = CategoriesEnums.TRAVEL;
        collectValue = travelMerchantsList;
        Collector travelUSIcodesCollector = new Collector("travel_US_Icodes_Collector", true, CollectType.MERCHANT_LIST, collectValue , MerchantLocation.US, targetAudienceLocationsAll , DealFetchersEnum.ICODES, categoryToBeSavedWith );
        collectors.add(travelUSIcodesCollector);
        Collector travelUKIcodesCollector = new Collector("travel_UK_Icodes_Collector", true, CollectType.MERCHANT_LIST, collectValue , MerchantLocation.UK, targetAudienceLocationsAll , DealFetchersEnum.ICODES, categoryToBeSavedWith );
        collectors.add(travelUKIcodesCollector);
//        Collector travelUSYieldCollector = new Collector("travel_US_Yield_Collector", true, CollectType.MERCHANT_LIST, collectValue , MerchantLocation.US, targetAudienceLocationsAll , DealFetchersEnum.YIELD, categoryToBeSavedWith );
//        collectors.add(travelUSYieldCollector);


//		//Travel collected by category
//		Collector travelUSIcodesCollector = new Collector("travel_US_Icodes_Collector", true, CollectType.CATEGORY, "Travel_and_Vacations" , MerchantLocation.US, targetAudienceLocationsAll , DealFetchersEnum.ICODES, categoryToBeSavedWith );
//		collectors.add(travelUSIcodesCollector);
//		Collector travelUKIcodesCollector = new Collector("travel_UK_Icodes_Collector", true, CollectType.CATEGORY, "Travel" , MerchantLocation.UK, targetAudienceLocationsAll , DealFetchersEnum.ICODES, categoryToBeSavedWith );
//		collectors.add(travelUKIcodesCollector);
//		Collector travelUSYieldCollector = new Collector("travel_US_Yield_Collector", true, CollectType.MERCHANT_LIST, collectValue , MerchantLocation.US, targetAudienceLocationsAll , DealFetchersEnum.YIELD, categoryToBeSavedWith );
//		collectors.add(travelUSYieldCollector);

        //TRAVEL Groupon US
        collectValue = "tour-travel";
        ArrayList<AudienceLocationEnum> targetAudienceLocationsUS = new ArrayList<AudienceLocationEnum>();
        targetAudienceLocationsUS.add(AudienceLocationEnum.US);
        Collector travelUSGrouponCollector = new Collector("travel_US_Groupon_Collector", true, CollectType.CATEGORY, collectValue , MerchantLocation.US, targetAudienceLocationsUS , DealFetchersEnum.GROUPON_US, categoryToBeSavedWith );
        collectors.add(travelUSGrouponCollector);

        //TRAVEL Groupon IL
        collectValue = "3";
        ArrayList<AudienceLocationEnum> targetAudienceLocationsIL = new ArrayList<AudienceLocationEnum>();
        targetAudienceLocationsIL.add(AudienceLocationEnum.IL);
        Collector travelILGrouponCollector = new Collector("travel_IL_Groupon_Collector", true, CollectType.CATEGORY, collectValue , MerchantLocation.IL, targetAudienceLocationsIL , DealFetchersEnum.GROUPON_IL, CategoriesEnums.TRAVEL_GROUPON );
        collectors.add(travelILGrouponCollector);

        return collectors;
    }
}

