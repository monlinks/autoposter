package com.monlinks.dealsposter.utils;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadDelay {

	private static Random random = new Random();
	private static final Logger log = LoggerFactory.getLogger(ThreadDelay.class);

	
	public static void delayInSeconds(long seconds){
		try {
			log.info("delayInSeconds=" + seconds);
			Thread.sleep(seconds * 1000L);
		} catch (InterruptedException e) {
		log.error(e.getMessage(),e);
		}
	}
	
	public static void delayRandomInSeconds(long minimumDelaySeconds,int additionalRandomdelaySeconds){
		long totalDelay = minimumDelaySeconds + random.nextInt(additionalRandomdelaySeconds);
		log.info("delayRandomInSeconds= " + totalDelay);
		delayInSeconds(totalDelay);
	}
	
	public static void delayRandomInMinutes(int randomLimitDelayMinutes){
		int minutesDelay = random.nextInt(randomLimitDelayMinutes);
		log.info("delayRandomInMinutes= " + minutesDelay);
		delayInSeconds(minutesDelay * 60);
	}
	
	
}
