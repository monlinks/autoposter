package com.monlinks.dealsposter.controllers;

import java.util.Date;

/**
 * Created by lior on 02/04/2017.
 */
public class DealsStatus {


    private String readableDate;
    private int unpublished = 0;
    private int cleaned = 0;
    private int published = 0;
    private int failed = 0;

    public DealsStatus(int unpublished ,int cleaned, int published,  int failed) {
        readableDate = (new Date()).toString();
        this.cleaned = cleaned;
        this.failed = failed;
        this.published = published;
        this.unpublished = unpublished;
    }

    public int getCleaned() {
        return cleaned;
    }

    public void setCleaned(int cleaned) {
        this.cleaned = cleaned;
    }

    public int getFailed() {
        return failed;
    }

    public void setFailed(int failed) {
        this.failed = failed;
    }

    public int getPublished() {
        return published;
    }

    public void setPublished(int published) {
        this.published = published;
    }

    public String getReadableDate() {
        return readableDate;
    }

    public void setReadableDate(String readableDate) {
        this.readableDate = readableDate;
    }

    public int getUnpublished() {
        return unpublished;
    }

    public void setUnpublished(int unpublished) {
        this.unpublished = unpublished;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DealsStatus{");
        sb.append("cleaned=").append(cleaned);
        sb.append(", readableDate='").append(readableDate).append('\'');
        sb.append(", unpublished=").append(unpublished);
        sb.append(", published=").append(published);
        sb.append(", failed=").append(failed);
        sb.append('}');
        return sb.toString();
    }
}
