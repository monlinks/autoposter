package com.monlinks.dealsposter.controllers;

import com.monlinks.dealsposter.activity.ActivityAgregator;
import com.monlinks.dealsposter.activity.BaseActivity;
import com.monlinks.dealsposter.dao.CollectorsRepository;
import com.monlinks.dealsposter.dao.DealsRepository;
import com.monlinks.dealsposter.dao.PublishersRepository;
import com.monlinks.dealsposter.dealscleaner.DealsCleanerJobExecutor;
import com.monlinks.dealsposter.dealscollectorservice.DealsCollectorJobExecutor;
import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.collector.Collector;
import com.monlinks.dealsposter.model.publisher.Publisher;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ApplicationController {
	
	@Autowired
	private DealsRepository repository;
	@Autowired
	private PublishersRepository publishersRepository;
	@Autowired
	private CollectorsRepository collectorsRepository;
	
	private static Logger log = LogManager.getLogger(ApplicationController.class.getName());
	private static String[] posterIds = {"TRAVEL_CATEGORY_GROUPON","TRAVEL_LIST_ICODES_YIELD","FASHION_LIST_ICODES_YIELD"};
	private static List<String> listOfActions = new ArrayList<String>();

	public ApplicationController() {
		listOfActions.add("/collectors");
		listOfActions.add("/publishers");
		listOfActions.add("/activity");
		listOfActions.add("/deals/status");
		listOfActions.add("/deals/unpublished");
		listOfActions.add("/deals/published");
		listOfActions.add("/deals/failed");

	}

	@RequestMapping("/")
	public List<String> getListOfActions() {
		log.info("START request getListOfActions");

		return listOfActions;
	}

	@RequestMapping("/start/collectors")
	public String startCollectors() {
		log.info("START request startCollectors");
		return "startCollectors";
	}

	@RequestMapping("/stop/collectors")
	public String stopCollectors() {
		log.info("START request stopCollectors");
		return "stopCollectors";
	}

	@RequestMapping("/start/publishers")
	public String startPublishers() {
		log.info("START request startPublishers");
		return "startPublishers";
	}
	
	@RequestMapping("/stop/publishers")
	public String stopPublishers() {
		log.info("START request stopPublishers");
		return "stopPublishers";
	}

	@RequestMapping("/keepalive")
    public String keepalive() {
		log.info("START request keepalive");
        return "keepalive WORKED";
    }
	
	
	@RequestMapping("/publishers")
    public List<Publisher> publishers() {
		log.info("START request publishers");
        return publishersRepository.findAll() ;
    }
	
	@RequestMapping("/collectors")
    public List<Collector> collectors() {
		log.info("START request collectors");
        return collectorsRepository.findAll() ;
    }

	@RequestMapping("/activity")
	public List<BaseActivity> activity() {
		log.info("START request activity");
//		ActivityAgregator.INSTANCE.addActivity((new PublisherActivity()).example());
//		ActivityAgregator.INSTANCE.addActivity((new CollectorActivity()).example());
		return ActivityAgregator.INSTANCE.getActivityList() ;
	}
	
	@RequestMapping("deals/unpublished/{category}")
    public List<DealsList> unpublished(@PathVariable String category) {
		log.info("START request: deals/unpublished");
		Sort sortData = new Sort(Sort.Direction.ASC, "endDate");//make the early ended deal be first
		List<DealsList> retList = new ArrayList<DealsList>();
		
			//List<Deal> dealsFromDB = repository.findUnpublishedDealsByMerchantName("גרופון", sortData);
		    List<Deal> dealsFromDB= null;
		    if(category==null){
				dealsFromDB = repository.findAllUnpublishedDeals(sortData);
			}else{
				dealsFromDB = repository.findUnpublishedDealsByCategory(category,sortData);
			}

			DealsList deals =  new DealsList("UNPUBLISHED", dealsFromDB.size(), dealsFromDB);
			retList.add(deals);
		
		log.info("retList="+retList);
        return retList;
    }
	
	@RequestMapping("deals/unpublished/delete")
    public List<DealsList> unpublishedDelete() {
		log.info("START request: deals/unpublished/delete");
		Sort sortData = new Sort(Sort.Direction.ASC, "endDate");//make the early ended deal be first
		List<DealsList> retList = new ArrayList<DealsList>();
		//List<Deal> dealsFromDB = repository.findUnpublishedDealsByMerchantName("גרופון", sortData);
		List<Deal> dealsFromDB = repository.findAllUnpublishedDeals(sortData);
		DealsList deals =  new DealsList("DELETE", dealsFromDB.size(), dealsFromDB);
		retList.add(deals);


		deleteDeals(dealsFromDB);
		
		log.info("retList="+retList);
        return retList;
    }
	
	 private void deleteDeals(List<Deal> dealsFromDB){
		 log.info("Going to DELETE deals");
		 repository.delete(dealsFromDB);
		 log.info("deleted number of deals="+dealsFromDB.size());
		 log.info("deletedDeals="+dealsFromDB);
	 }




	@RequestMapping("deals/published")
	public DealsList publishedDeals() {
		log.info("START request: deals/published");
		List<Deal> dealsFromDB = repository.findAllPublishedDeals();
		DealsList deals =  new DealsList("PUBLISHED", dealsFromDB.size(), dealsFromDB);
		deals.changeDealDataToBeClickable();
		return deals;
	}

	@RequestMapping("deals/failed")
	public DealsList failed() {
		log.info("START request: failed");
		//List<Deal> dealsFromDB = repository.findByRegExp("affUrl","FAILED");
		List<Deal> dealsFromDB = repository.findAllFailedDeals();
		DealsList deals =  new DealsList("FAILED", dealsFromDB.size(), dealsFromDB);
		log.info("failed to post deals="+deals);
		return deals;
	}

	@RequestMapping("deals/cleanNow")
	public String cleanNow() {
		log.info("START request: cleanNow");
		DealsCleanerJobExecutor dealsCleanerJobExecutor = new DealsCleanerJobExecutor();
		dealsCleanerJobExecutor.runCleanDirectly(repository);
		return "DONE";
	}
	
	@RequestMapping("deals/failed/delete")
	public DealsList deleteFailed() {
		log.info("START request: deleteFailed");
		//List<Deal> dealsFromDB = repository.findByRegExp("affUrl","FAILED");
		List<Deal> dealsFromDB = repository.findAllFailedDeals();
		DealsList deals =  new DealsList("FAILED", dealsFromDB.size(), dealsFromDB);
		deleteDeals(dealsFromDB);
		return deals;
	}

	@RequestMapping("deals/status")
	public DealsStatus dealsStatus() {
		log.info("START request: DealsStatus");
		//TODO: improve this to get only the count without returning the full list
		List<Deal> unpublishedDealsFromDB = repository.findAllUnpublishedDeals(null);
		List<Deal> cleanedDealsFromDB = repository.findAllUnpublishedCleanedDeals();
		List<Deal> publishedDealsFromDB = repository.findAllPublishedDeals();
		List<Deal> failedDealsFromDB = repository.findAllFailedDeals();
		DealsStatus dealsStatus = new DealsStatus(unpublishedDealsFromDB.size(),cleanedDealsFromDB.size(),publishedDealsFromDB.size(),failedDealsFromDB.size());
		log.debug("dealsStatus="+dealsStatus);
		return dealsStatus;
	}
	
	@RequestMapping("/collectNow")
	public DealsList collectNow(@RequestParam(value="site", defaultValue="ALL") String site) {
		log.info("START request: collectNow , site="+site);
		List<Deal> dealsFromDB = null;
		DealsCollectorJobExecutor dealsCollectorJobExecutor = new DealsCollectorJobExecutor();
		if(site.equals("ALL")){
			log.debug("Collect now from all sources");
			List<Collector> collectors = collectorsRepository.findByEnabled(true);//get only enabled publishers from DB
			log.info("Collectors="+ collectors);
			if(collectors!=null && !collectors.isEmpty()){
				dealsFromDB = dealsCollectorJobExecutor.runCollectDirectly(collectors,repository,collectorsRepository);
			}
			else{
				log.error("No enabled collectors were found. Will not collect any deals.");
			}
		}else{
//			log.debug("Collect now from iCodes and YieldKit only");
//			IDealsFetcher iCodesFetcher = new ICodesDealsFetcher();
//			IDealsFetcher yieldkitDealsFetcher = new YieldkitDealsFetcher();
//			CollectSource collectSource = new CollectSource(CollectType.MERCHANT_LIST, site , MerchantLocation.ALL,new IDealsFetcher[]{iCodesFetcher,yieldkitDealsFetcher});
//			dealsFromDB = Application.dealsCollectorJob.handleCollectByList(collectSource);
		}


		DealsList deals =  new DealsList(site, dealsFromDB.size(), dealsFromDB);
		log.info("FINISH request: collectNow");
		return deals;
	}


	
//	@RequestMapping("/publishNow")
//	public DealsList publishNow() {
//		log.info("START request: publishNow");
//		List<Deal> dealsFromDB = Application.
//		DealsList deals =  new DealsList("ALL", dealsFromDB.size(), dealsFromDB);
//		log.info("failed to post deals="+deals);
//		return deals;
//	}
	
//TODO: more endpoints
//ACTIONS:
//collectnow
//postnow?numberOfPosts=5&posterId
//READ only:
//facebooktoken
//nextrun
}
