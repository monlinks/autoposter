package com.monlinks.dealsposter.controllers;

import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.publisher.PublishAction;

import java.util.ArrayList;
import java.util.List;

public class DealsList {
    private String posterId = null;
    private int listSize = 0;
    private List<Deal> listOfDeals = null;

    public DealsList(String posterId, int listSize, List<Deal> listOfDeals) {
        super();
        this.posterId = posterId;
        this.listSize = listSize;
        this.listOfDeals = listOfDeals;
    }

    public void changeDealDataToBeClickable() {
        for (Deal deal : listOfDeals) {

            String affUrl = deal.getAffUrl();
            if(affUrl != null && affUrl.contains("goo.gl")) {
                deal.setAffUrl(affUrl+"+");//make it clickable to link statistics
            }

            //update the link to the published post
            ArrayList<PublishAction> actions = deal.getPostedTo();
            for (PublishAction publishAction : actions ) {
                String destinationId = publishAction.getDestinationId();
                String postId = publishAction.getPostId();
                publishAction.setPostId("https://www.facebook.com/groups/"+destinationId+"/permalink/"+postId );

            }

        }
    }

    public String getPosterId() {
        return posterId;
    }

    public void setPosterId(String posterId) {
        this.posterId = posterId;
    }

    public int getListSize() {
        return listSize;
    }

    public void setListSize(int listSize) {
        this.listSize = listSize;
    }

    public List<Deal> getListOfDeals() {
        return listOfDeals;
    }

    public void setListOfDeals(List<Deal> listOfDeals) {
        this.listOfDeals = listOfDeals;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DealsList [posterId=");
        builder.append(posterId);
        builder.append(", listSize=");
        builder.append(listSize);
        builder.append(", listOfDeals=");
        builder.append(listOfDeals);
        builder.append("]");
        return builder.toString();
    }


}
