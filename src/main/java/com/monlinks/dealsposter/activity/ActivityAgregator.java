package com.monlinks.dealsposter.activity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lior on 13/04/2017.
 */
public class ActivityAgregator {

public static final ActivityAgregator INSTANCE = new ActivityAgregator();
private List<BaseActivity> activityList = new ArrayList<BaseActivity>();

    private ActivityAgregator() {  }

    public void addActivity(BaseActivity activityToAdd){
        activityList.add(activityToAdd);
    }

    public List<BaseActivity> getActivityList() {
        return activityList;
    }
}
