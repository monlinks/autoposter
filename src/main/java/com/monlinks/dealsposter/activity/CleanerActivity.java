package com.monlinks.dealsposter.activity;

/**
 * Created by lior on 13/04/2017.
 */
public class CleanerActivity extends BaseActivity {
    private final String type = "CLEANER";
    private String testCleanStr;
    private int numOfCleanedDeals;


    public CleanerActivity(String name, String dateTimeStr, String testCleanStr, int numOfCleanedDeals) {
        super(name, dateTimeStr);
        this.testCleanStr = testCleanStr;
        this.numOfCleanedDeals = numOfCleanedDeals;
    }

    public int getNumOfCleanedDeals() {
        return numOfCleanedDeals;
    }
    public void setNumOfCleanedDeals(int numOfCleanedDeals) {
        this.numOfCleanedDeals = numOfCleanedDeals;
    }
    public String getType() {
        return type;
    }

    public String getTestCleanStr() {
        return testCleanStr;
    }

    public void setTestCleanStr(String testCleanStr) {
        this.testCleanStr = testCleanStr;
    }


}