package com.monlinks.dealsposter.activity;

import java.util.Date;

/**
 * Created by lior on 13/04/2017.
 */
public class CollectorActivity extends BaseActivity {
    private final String type = "COLLECTOR";
    private int numOfFoundDeals;
    private int numOfNewSavedDeals;

    public CollectorActivity(String name, String dateTimeStr, int numOfFoundDeals, int numOfNewSavedDeals) {
        super(name, dateTimeStr);
        this.numOfFoundDeals = numOfFoundDeals;
        this.numOfNewSavedDeals = numOfNewSavedDeals;
    }

    public CollectorActivity() {

    }

    public int getNumOfFoundDeals() {
        return numOfFoundDeals;
    }

    public void setNumOfFoundDeals(int numOfFoundDeals) {
        this.numOfFoundDeals = numOfFoundDeals;
    }

    public int getNumOfNewSavedDeals() {
        return numOfNewSavedDeals;
    }

    public void setNumOfNewSavedDeals(int numOfNewSavedDeals) {
        this.numOfNewSavedDeals = numOfNewSavedDeals;
    }

    public String getType() {
        return type;
    }

    public CollectorActivity example() {
        return new CollectorActivity("Example1",(new Date()).toString(),100,5);
    }


}
