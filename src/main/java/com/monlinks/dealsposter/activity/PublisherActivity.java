package com.monlinks.dealsposter.activity;

import java.util.Date;

/**
 * Created by lior on 13/04/2017.
 */
public class PublisherActivity extends BaseActivity {
    private final String type = "PUBLISHER";
    private String destName;
    private String destId;
    private int numOfSucPublishedDeals;
    private int numOfFailedPublishedDeals;

    public PublisherActivity(String name, String dateTimeStr, String destName, String destId, int numOfSucPublishedDeals, int numOfFailedPublishedDeals) {
        super(name, dateTimeStr);
        this.destName = destName;
        this.destId = destId;
        this.numOfSucPublishedDeals = numOfSucPublishedDeals;
        this.numOfFailedPublishedDeals = numOfFailedPublishedDeals;
    }

    public PublisherActivity() {
        super();
    }

    public String getDestName() {
        return destName;
    }

    public void setDestName(String destName) {
        this.destName = destName;
    }

    public String getDestId() {
        return destId;
    }

    public void setDestId(String destId) {
        this.destId = destId;
    }

    public int getNumOfSucPublishedDeals() {
        return numOfSucPublishedDeals;
    }

    public void setNumOfSucPublishedDeals(int numOfSucPublishedDeals) {
        this.numOfSucPublishedDeals = numOfSucPublishedDeals;
    }

    public int getNumOfFailedPublishedDeals() {
        return numOfFailedPublishedDeals;
    }

    public void setNumOfFailedPublishedDeals(int numOfFailedPublishedDeals) {
        this.numOfFailedPublishedDeals = numOfFailedPublishedDeals;
    }

    public String getType() {
        return type;
    }

    public PublisherActivity example() {
        return new PublisherActivity("Example",(new Date()).toString(),"destName1","destId1",2,3);
    }
}
