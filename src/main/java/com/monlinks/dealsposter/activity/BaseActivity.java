package com.monlinks.dealsposter.activity;

/**
 * Created by lior on 13/04/2017.
 */
public class BaseActivity {
    static String type;
    private String name;
    private String dateTimeStr;

    public BaseActivity(String name, String dateTimeStr) {
        this.name = name;
        this.dateTimeStr = dateTimeStr;
    }

    public BaseActivity() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateTimeStr() {
        return dateTimeStr;
    }

    public void setDateTimeStr(String dateTimeStr) {
        this.dateTimeStr = dateTimeStr;
    }
}
