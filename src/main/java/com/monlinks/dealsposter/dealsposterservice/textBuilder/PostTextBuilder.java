package com.monlinks.dealsposter.dealsposterservice.textBuilder;

import com.monlinks.dealsposter.dealsposterservice.postsender.DealPostData;
import com.monlinks.dealsposter.model.publisher.PostingDestination.PostLangEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;

public class PostTextBuilder {
	
	private String footerText;
	private String messageText;
	private PostLangEnum postLang;
	
	private static final Logger log = LoggerFactory.getLogger(PostTextBuilder.class);
	
	private String couponMessageTemplate = "";
	private String offerMessageTemplate = "";
	private String noDateStr = "";
	private String couponPrefixTemplate = "";
	private String xdaysLeftTemplate="";
	private String oneDayLeft= "" ;
	private String hoursLeft= "" ;
	private String validUntileTemplate = "";
	private String goodWord = "";
	private String[] goodWordArr;


	public PostTextBuilder(DealPostData postDataObj, PostLangEnum postLang) {
		this.postLang = postLang;
		Locale currentLocale = PostLangEnum.ENG.equals(postLang)?Locale.ENGLISH:Locale.JAPANESE;//No Hebrew support, so will take default==Hebrew
		
		//load relevant resource bundle for this lang
		//TODO: make it more efficient - load the messages only onec for language
		ResourceBundle messages = ResourceBundle.getBundle("messages", currentLocale);
		
		//update the string per language
		updateStrings(messages);
		
		setFooterText(buildFooterText(postDataObj));
		setMessageText(buildMessageText(postDataObj));
	}
	
	private void updateStrings(ResourceBundle messages) {
		couponMessageTemplate = getUtf8Str(messages,"couponMessageTemplate");//messages.getString("couponMessageTemplate");
		offerMessageTemplate = getUtf8Str(messages,"offerMessageTemplate");
		noDateStr = getUtf8Str(messages,"noDateStr");
		couponPrefixTemplate = getUtf8Str(messages,"couponPrefixTemplate");
		xdaysLeftTemplate=getUtf8Str(messages,"xdaysLeftTemplate");
		oneDayLeft= getUtf8Str(messages,"oneDayLeft");
		hoursLeft= getUtf8Str(messages,"hoursLeft");
		validUntileTemplate = getUtf8Str(messages,"validUntileTemplate");
		goodWord = getUtf8Str(messages,"goodWord");
		goodWordArr = goodWord.split(",");
	}

	private String getUtf8Str(ResourceBundle messages, String msgKey) {
		// TODO Auto-generated method stub
		String valFromBundle = messages.getString(msgKey);
		try {
			valFromBundle =  new String(valFromBundle.getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return valFromBundle;
	}

	private String buildMessageText(DealPostData postDataObj) {
		String retText = "";
		String merchantName = makeItNotLinkable(postDataObj.getMerchantName());
		String dealDescription = makeItNotLinkable(postDataObj.getDescription());
		if(postDataObj.getCode() == null || postDataObj.getCode().isEmpty()){
			//נהדר , שווה, משתלם , לזמן מוגבל , 
			//retText="מבצע באתר "+ makeItNotLinkable(postDataObj.getMerchantName())+", לפרטים נוספים הכנסו מכאן לאתר.\nתאור המבצע:\n"+ makeItNotLinkable(postDataObj.getDescription());
			String randomGoodWord = calcRandomGoodWord();
			retText= MessageFormat.format(offerMessageTemplate, randomGoodWord , merchantName,dealDescription);
		}else{
			//retText="קוד קופון לאתר "+ makeItNotLinkable(postDataObj.getMerchantName())+", העתיקו את קוד הקופון "+postDataObj.getCode()+" והכנסו מכאן לאתר.\nתאור הקופון:\n"+ makeItNotLinkable(postDataObj.getDescription());
			retText= MessageFormat.format(couponMessageTemplate,merchantName,postDataObj.getCode(),dealDescription);
		}
		
		log.debug("buildMessageText = "+ retText);
		return retText;
	}

	private String calcRandomGoodWord() {
		int rnd = new Random().nextInt(goodWordArr.length);
		return goodWordArr[rnd];
	}

	private String makeItNotLinkable(String inputStr) {
		String retStr = inputStr;
		if(inputStr != null){
			retStr = inputStr.replaceAll(".com", " .com").replaceAll(".co.il", " .co.il").replaceAll(".co.il", " .co.il");
		}
		return retStr;
	}
	
	private String buildFooterText(DealPostData postDataObj) {
		String retText = "";
		
		String noDate = noDateStr;
		//String startDate = postDataObj.getStartDate() == null?noDate:postDataObj.getStartDate();
		String endDate = postDataObj.getEndDate()== null?noDate:postDataObj.getEndDate();
		
		if(postDataObj.getCode() != null && !postDataObj.getCode().isEmpty()){
			retText = MessageFormat.format(couponPrefixTemplate, postDataObj.getCode()); //"קוד קופון: "+ postDataObj.getCode()+" | ";//couponPrefixTemplate
		}
		
		String validTimeTemplate = MessageFormat.format(validUntileTemplate, endDate); //"בתוקף עד: "+endDate;//validUntileTemplate
		
		String daysLeft = "";
		if(postDataObj.getDaysToExpiration()>1 && postDataObj.getDaysToExpiration()<8){
			daysLeft = MessageFormat.format(xdaysLeftTemplate, postDataObj.getDaysToExpiration()); //" .נותרו רק עוד " + postDataObj.getDaysToExpiration() + " ימים";//xdaysLeftTemplate
		}
		if(postDataObj.getDaysToExpiration()==1){
			daysLeft =  oneDayLeft; //" .נותר רק עוד יום אחד!";//oneDayLeft
		}
		if(postDataObj.getDaysToExpiration()==0){
			daysLeft =  hoursLeft;
		}
		
		retText = retText + validTimeTemplate + daysLeft;
		
		log.debug("buildFooterText = "+ retText);
		return retText;
	}
	
	public String getFooterText() {
		return footerText;
	}
	public void setFooterText(String footerText) {
		this.footerText = footerText;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PostTextBuilder [footerText=");
		builder.append(footerText);
		builder.append(", messageText=");
		builder.append(messageText);
		builder.append(", postLang=");
		builder.append(postLang);
		builder.append(", couponMessageTemplate=");
		builder.append(couponMessageTemplate);
		builder.append(", offerMessageTemplate=");
		builder.append(offerMessageTemplate);
		builder.append(", noDateStr=");
		builder.append(noDateStr);
		builder.append(", couponPrefixTemplate=");
		builder.append(couponPrefixTemplate);
		builder.append(", xdaysLeftTemplate=");
		builder.append(xdaysLeftTemplate);
		builder.append(", oneDayLeft=");
		builder.append(oneDayLeft);
		builder.append(", validUntileTemplate=");
		builder.append(validUntileTemplate);
		builder.append(", goodWord=");
		builder.append(goodWord);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	

}
