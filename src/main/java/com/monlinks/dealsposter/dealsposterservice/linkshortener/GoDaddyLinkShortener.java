package com.monlinks.dealsposter.dealsposterservice.linkshortener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.monlinks.dealsposter.utils.ImageExtractor;
import com.monlinks.dealsposter.utils.UrlRelatedHelper;

public class GoDaddyLinkShortener implements LinkShortener {

	private static final Logger log = LoggerFactory.getLogger(GoDaddyLinkShortener.class);
	private static String  goDaddyShortenerUrlPattern = "https://shortener.godaddy.com/v1/?apikey=2ec128aef9c511e6a331fa163eda3b0d&url=";
	
	
	public String makeUrlShort(String longUrl){
		log.debug("longUrl="+longUrl);
		String shortenerUrl = goDaddyShortenerUrlPattern+longUrl;
		
		String shortUrl = null;//use the long URL in case we fail
		try {
			shortUrl = UrlRelatedHelper.getHTML(shortenerUrl);
			if(shortUrl == null || shortUrl.isEmpty()){
				shortUrl = longUrl;
			}
		} catch (Exception e) {
			log.error("Problem in getting url="+shortenerUrl,e);
		}

		log.debug("shortUrl="+shortUrl);
		return shortUrl;
				
	}

}
