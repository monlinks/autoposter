package com.monlinks.dealsposter.dealsposterservice;

import com.monlinks.dealsposter.activity.ActivityAgregator;
import com.monlinks.dealsposter.activity.PublisherActivity;
import com.monlinks.dealsposter.dao.DealsRepository;
import com.monlinks.dealsposter.dao.PublishersRepository;
import com.monlinks.dealsposter.dealsposterservice.affiliation.AffiliateBuilder;
import com.monlinks.dealsposter.dealsposterservice.linkshortener.GooLinkShortener;
import com.monlinks.dealsposter.dealsposterservice.linkshortener.LinkShortener;
import com.monlinks.dealsposter.dealsposterservice.postsender.DealPostData;
import com.monlinks.dealsposter.dealsposterservice.postsender.FacebookPostSender;
import com.monlinks.dealsposter.dealsposterservice.postsender.IPostSender;
import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.collector.CollectType;
import com.monlinks.dealsposter.model.publisher.*;
import com.monlinks.dealsposter.model.publisher.PostingDestination.SocialNetworksEnum;
import com.monlinks.dealsposter.utils.AffiliationUrlNotValidException;
import com.monlinks.dealsposter.utils.DateTimeHelper;
import com.monlinks.dealsposter.utils.ThreadDelay;
import com.monlinks.dealsposter.utils.UrlRelatedHelper;
import facebook4j.FacebookException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.quartz.CronExpression;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;


public class DealsPublisherJobExecutor implements Job {
	private DealsRepository repository;
	private PublishersRepository publishersRepository;

	private boolean isRunningInTestMode = false;
	private static LinkShortener linkShortener = new GooLinkShortener();
	
	private static Logger log = LogManager.getLogger(DealsPublisherJobExecutor.class.getName());
	private ResourceBundle config = ResourceBundle.getBundle("application",Locale.ROOT);

	private  int delayBetweenPostsSec = 1;
	private  int randomDelayBeforeStartMinutes = 1;
	private  int extraRandomdelayBetweenPostsSec = 1;
	


	public DealsPublisherJobExecutor() {
		isRunningInTestMode = Boolean.parseBoolean(config.getString("publishDeals.test.mode"));
		if(!isRunningInTestMode){
			delayBetweenPostsSec = Integer.parseInt(config.getString("publishDeals.delay.between.posts.sec"));
			extraRandomdelayBetweenPostsSec = Integer.parseInt(config.getString("publishDeals.extra.random.delay.between.posts.sec"));
			randomDelayBeforeStartMinutes = Integer.parseInt(config.getString("publishDeals.delay.random.before.start.minutes"));
		}
	
	}


		
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("============== START DealsPublisherJobExecutor ================");
		if(isRunningInTestMode) log.info("RUNNING IN TEST MODE");
		
		try{
				
			String publisherId = (String) context.getJobDetail().getJobDataMap().get("publisherId");
			this.repository = (DealsRepository) context.getJobDetail().getJobDataMap().get("dealsRepository");
			this.publishersRepository = (PublishersRepository) context.getJobDetail().getJobDataMap().get("publishersRepository");
			
			//publisher may change in DB since first load. Need to take it again from DB
			Publisher publisher = publishersRepository.findOne(publisherId);
			
			if(publisher==null){
				log.info("Publisher with id="+ publisherId +" was not found in DB. Will not publish any deal.");
				return;
			}
			//if publisher is disabled stop and return
			if(!publisher.isEnabled()){
				log.info("Publisher "+ publisher.getName()+" is DISABLED. Will not publish any deal.");
				return;
			}

			log.info("randomDelayBeforeStartMinutes="+randomDelayBeforeStartMinutes);
			ThreadDelay.delayRandomInMinutes(randomDelayBeforeStartMinutes);//will create random delay 0-15 minutes

			

			//Create Facebook posting user
			//TODO: build sender according to: publisherJob.getPostingDestination().getSocialNetwork();
			IPostSender facebookSender = new FacebookPostSender(publisher.getPostingUser());

			for (PublisherJob publisherJob : publisher.getPublisherJobs()) {
				handlePublishJob(publisherJob,facebookSender,publisher.getName());
			}

			log.info("============== FINISH DealsPublisherJobExecutor ================");

			//TODO: Fix this print - printNextRun(context);
		}catch(Exception e){
			log.error(e.getMessage(),e);
		}

	}
		


	private void handlePublishJob(PublisherJob publisherJob, IPostSender facebookSender, String publisherUserId) {
		log.info("START publisherJob: "+ publisherJob.getPostingDestination().getDestinationName());
		List<Deal> publishedDeals = new ArrayList<Deal>();
		int succCounter = 0;
		int failedCounter = 0;

		try {
			//get the relevant deals from DB
			List<Deal> unpublishedDeals = getUnpublishDeals(publisherJob);

			//publish all deals
			PublishAction publishAction = null;
			for (Deal deal : unpublishedDeals) {
				publishAction = publishTheDeal(deal,facebookSender,publisherJob.getPostingDestination(),publisherUserId);//will also add publishAction to the deal
				log.debug("After publish deal "+ deal.getExtId() +" => publishAction="+ publishAction);
				publishedDeals.add(deal);
				saveTheDeal(deal);

				//delay between posts only if post was successfull
				if(publishAction.getActionResult().equals(ActionResultEnum.SUCCESS)){
					ThreadDelay.delayRandomInSeconds(delayBetweenPostsSec, extraRandomdelayBetweenPostsSec);
					succCounter++;
				}else{
					failedCounter++;
				}
			}
			

			log.info("FINISH publisherJob: "+ publisherJob.getPostingDestination().getDestinationName() +" ,Found "+ unpublishedDeals.size() +" unpublished deals. Saved "+ publishedDeals.size() +" published deals.");
			ActivityAgregator.INSTANCE.addActivity(new PublisherActivity(publisherJob.getPublisherName(),(new Date()).toString(),publisherJob.getPostingDestination().getDestinationName(),publisherJob.getPostingDestination().getDestinationId(),succCounter,failedCounter));


		} catch (Exception e) {
			log.error("Failed to run publisherJob="+ publisherJob,e);
		}
	}

	private void saveTheDeal(Deal deal) {
		/*********** SAVE to DB ***********/

		log.debug("Save published deal to DB. Deal id="+deal.getExtId());
		repository.save(deal);

		/**********************************/
	}

	public PublishAction publishTheDeal(Deal deal, IPostSender facebookSender, PostingDestination postingDestination, String publisherUserId) {
		log.debug("********** START: handle deal "+ deal.getExtId() +" for website "+ deal.getMerchant().getHomePageUrl() +"****************");

		AffiliateBuilder affiliateBuilder = new AffiliateBuilder();
		//prepair data for publishAction obj
		String postDate = LocalDateTime.now().toString();
		String errorDescription = "";
		String destinationId = postingDestination.getDestinationId();
		String destinationName = postingDestination.getDestinationName();
		SocialNetworksEnum socialNetwork = postingDestination.getSocialNetwork();
		ActionResultEnum actionResult = ActionResultEnum.SUCCESS;

		String publishedAffLink = null;
		String publishedShortAffLink = null;
		PublishAction publishAction = null;
        String postId = null;

		try {
			int hoursToExpiration = hoursToExpiration(deal);
			log.debug("hoursToExpiration=" + hoursToExpiration);

			if (hoursToExpiration > 0) {
				DealPostData dealToPost = new DealPostData(deal);
				dealToPost.setAffUrl(affiliateBuilder.buildAffUrl(publisherUserId,dealToPost.getDeepLink()));
				publishedAffLink = dealToPost.getAffUrl();
				log.debug("publishedAffLink="+publishedAffLink);

				//checkDealPostDataValidity(dealToPost);
				dealToPost.setDaysToExpiration(hoursToExpiration / 24);//convert hours to days
				//publishedShortAffLink = shortenAffLink(dealToPost);
				//log.debug("publishedShortAffLink="+publishedShortAffLink);

				log.debug("dealToPost=" + dealToPost);

				//******************* POST TO FACEBOOK ***********/
				//TODO: return facebook postID and add it to publishAction result
				postId = facebookSender.postTheDeal(dealToPost, postingDestination);

				//************************************************/

				//deal.setAffUrl(dealToPost.getAffUrl());
				log.debug("Deal was published: " + deal);

			} else {
				log.debug("Deal is expired. will not be published." + deal);
				actionResult = ActionResultEnum.FAILED;
				errorDescription = "Deal " + deal.getExtId() + " is Expiered.";
				//we delete the deal only in case of expiration
				//repository.delete(deal.getExtId());
			}
		} catch (FacebookException e) {
			actionResult = ActionResultEnum.FAILED;
			errorDescription = "FacebookException: code"+ e.getErrorCode();
			log.error(errorDescription,e);
		} catch (MalformedURLException e) {
			actionResult = ActionResultEnum.FAILED;
			errorDescription="MalformedURLException:"+ e.getMessage();
			log.error(errorDescription,e);
		} catch (AffiliationUrlNotValidException e) {
			actionResult = ActionResultEnum.FAILED;
			errorDescription="AffiliationUrlNotValidException:"+ e.getMessage();
			log.error(errorDescription,e);
		} catch (Exception e) {
			actionResult = ActionResultEnum.FAILED;
			errorDescription="Exception:"+ e.getMessage();
			log.error(errorDescription,e);
		}


		publishAction = new PublishAction(socialNetwork, postDate , postId,actionResult, errorDescription, destinationId, destinationName,publishedAffLink,publishedShortAffLink );
		deal.getPostedTo().add(publishAction );
		log.debug("Deal with Action: " + deal);
			
		log.debug("********** END: handle deal "+ deal.getExtId() +" ****************");
		
		return publishAction;
		
	}

	private String  shortenAffLink(DealPostData dealToPost) {
		String longLink = dealToPost.getAffUrl();
		log.debug("longLink="+longLink);
		String shortLink = longLink;
		//call the link shortener API only if the URL is not already short
		//this case will happened when a deal try to be published again to another destination
		if(!longLink.contains("goo.gl")) {
			shortLink = linkShortener.makeUrlShort(longLink);
		}
		log.debug("shortLink=" + shortLink);
		dealToPost.setAffUrl(shortLink);
		return  shortLink;
	}

	private void checkDealPostDataValidity(DealPostData dealToPost) throws AffiliationUrlNotValidException, MalformedURLException {
		//currenly will run only for the following domains: prosperent.com,r.srvtrck.com/
		if(dealToPost.getAffUrl().contains("groupon")){
			log.warn("Validation will not be performed on Groupon links.");
			return;
		}

		String merchantDomain = dealToPost.getDomainName();
		log.debug("merchantDomain="+merchantDomain);
		String affUrl = dealToPost.getAffUrl();
		log.debug("affUrl="+affUrl);
		String finalUrl = UrlRelatedHelper.calcFinalAffUrl2(affUrl);
		log.debug("finalUrl="+finalUrl);
		if(finalUrl == null){
			//if the final url was not successsfuly calculated stop the validation
			return;
		}


		URL finalUrlObj = new URL(finalUrl);
		String finalUrlDomain = finalUrlObj.getHost();
		//if merchantDomain != finalUrlDomain then the affiliate URL is not working well
		if(!finalUrlDomain.contains(merchantDomain)){
			String err = "Broken affiliate URL! Final url domain="+finalUrlDomain+" does not contains merchantDomain="+merchantDomain;
			throw new AffiliationUrlNotValidException(err,affUrl,finalUrl,0);
		}else{
			log.debug("Aff url redirect correctly.affUrl="+affUrl );
		}
	}

	private List<Deal> getUnpublishDeals(PublisherJob publisherJob) {
		List<Deal> unpublishedDeals = null;
		DealsSelector dealsSelector = publisherJob.getDealsSelector();
		String destinationId = publisherJob.getPostingDestination().getDestinationId();
	
		Sort sortData = new Sort(Direction.ASC, "endDate");//make the early ended deal be first
				
		Pageable pagingData = new PageRequest(0, dealsSelector.getDealsBulkSize(),sortData);//will take only the first dealsBulkSize

		String audienceLocationToFind = dealsSelector.getAudienceLocation().name();
		String destinationIdNotPublished = destinationId;

		CollectType selectorType = dealsSelector.getSelectorType();
		if(selectorType.equals(CollectType.CATEGORY)){
			String categoryToFind = dealsSelector.getSelectString();//the category name
			log.debug("Going to find in DB: categoryToFind="+categoryToFind+", audienceLocationToFind="+audienceLocationToFind+",destinationIdNotPublished="+destinationIdNotPublished);
			unpublishedDeals = repository.findUnpublishedDealsForThisPublishJobByCategory(categoryToFind, audienceLocationToFind, destinationIdNotPublished,pagingData);//TODO: add merchant location
		}else if(selectorType.equals(CollectType.MERCHANT_LIST)){
			String merchantListStr = dealsSelector.getSelectString();//list as string
			log.debug("Going to find in DB: merchantList="+merchantListStr+", audienceLocationToFind="+audienceLocationToFind+",destinationIdNotPublished="+destinationIdNotPublished);
			String regExpListWithOr = merchantListStr.replace(",","|");
			log.debug("regExpListWithOr="+regExpListWithOr);
			unpublishedDeals = repository.findUnpublishedDealsForThisPublishJobByList(regExpListWithOr, audienceLocationToFind, destinationIdNotPublished,pagingData);//TODO: add merchant location
		}

		log.info("unpublishedDeals.size() = "+ unpublishedDeals.size());
		
		return unpublishedDeals;
	}


	private void printNextRun(JobExecutionContext context) {
		String publishScheduleCronStr = "";
		try {
			publishScheduleCronStr = (String) context.getJobDetail().getJobDataMap().get("publishScheduleCronStr");
			log.debug("publishScheduleCronStr="+publishScheduleCronStr);
			
			CronExpression exp;
			exp = new CronExpression(publishScheduleCronStr);
			Date nextRun = exp.getNextValidTimeAfter(new Date());
			log.info("DealsPublisherJob nextRun="+nextRun);
			
			log.info("DealsPublisherJob nextRun ISRAEL time="+ DateTimeHelper.calcIsraelTime(nextRun));
		} catch (ParseException e) {
			log.error("Problem in cron string="+ publishScheduleCronStr , e);
		}

	}


	private int hoursToExpiration(Deal deal) {
		int hoursDiff = 1;
		try {
			Date currentDate = new Date();
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date dateFromDeal = dt.parse(deal.getEndDate());
			long timeDiff = dateFromDeal.getTime() - currentDate.getTime();
			hoursDiff = (int) (timeDiff / 1000 /60 /60) ;
		} catch (ParseException e) {
			log.warn("Problem in parse date:"+deal.getEndDate(),e);
		}

		return hoursDiff;
	}

	private int daysToExpiration(Deal deal) {
		int daysDiff = 1;
		try {
			Date currentDate = new Date();
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date dateFromDeal = dt.parse(deal.getEndDate());
			long timeDiff = dateFromDeal.getTime() - currentDate.getTime();
			daysDiff = (int) (timeDiff / 1000 /60 /60 /24) ;
		} catch (ParseException e) {
			log.warn("Problem in parse date:"+deal.getEndDate(),e);
		}
	
		return daysDiff;
	}

	
	public int getDelayBetweenPostsSec() {
		return delayBetweenPostsSec;
	}

	public void setDelayBetweenPostsSec(int delayBetweenPostsSec) {
		this.delayBetweenPostsSec = delayBetweenPostsSec;
	}


	public boolean isRunningInTestMode() {
		return isRunningInTestMode;
	}

	public void setRunningInTestMode(boolean runningInTestMode) {
		isRunningInTestMode = runningInTestMode;
	}



}
