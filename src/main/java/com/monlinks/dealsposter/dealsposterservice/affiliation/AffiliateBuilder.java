package com.monlinks.dealsposter.dealsposterservice.affiliation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lior on 30/05/2017.
 */
public class AffiliateBuilder {

    private static final Logger log = LoggerFactory.getLogger(AffiliateBuilder.class);

    Map<String,String> mapUserIdToPublisherId = new HashMap<String,String>();

    private static String VIG_URL_TEMPLATE = "http://redirect.viglink.com?key=%s&u=%s";
    //SKIM => http://go.redirectingat.com/?id=[[PUB_ID]]&xs=1&url=[[DEEP_URL]]&xcust=[[SHARE_LINK_ID]]&sref=[[CURRENT_LOCATION_URL]]"
    private static String SKIM_URL_TEMPLATE = "http://go.redirectingat.com/?id=%s&xs=1&url=%s";

    public AffiliateBuilder() {
        mapUserIdToPublisherId.put("ushops","104668X1561421");//skimlinks
        mapUserIdToPublisherId.put("monlinksPublisher","10a309a920033f8b2c751717059df529");//viglink campain tormim.com
    }

    public String buildAffUrl(String userId, String dealDeepLinkUrl){
        //TODO: change it to polimorfism
        String pubId = mapUserIdToPublisherId.get(userId);
        String currentTemplate = null;
        String retAffUrl = null;
        if(userId.equals("ushops")){
            currentTemplate = SKIM_URL_TEMPLATE;
        }else{
            currentTemplate = VIG_URL_TEMPLATE;
        }

        retAffUrl = buildFinalAffUrl(currentTemplate,pubId,dealDeepLinkUrl);
        return retAffUrl;
    }

    private String buildFinalAffUrl(String currentTemplate, String pubId,String deepLink) {
        log.debug("buildFinalAffUrl");
        //TODO: make this prity
        if(deepLink.contains("groupon")) return deepLink;//This is for groupon deals that comes with affiliate link
        String retAffLink = String.format(currentTemplate, pubId , URLEncoder.encode(deepLink));
        if(currentTemplate.contains("go.redirectingat.com")){
            retAffLink = retAffLink +"&sref=http%3A%2F%2Ffacebook.com%2Fushops%2Ffashion";
        }
        log.debug("retAffLink="+ retAffLink);
        return retAffLink;
    }

//    public String buildAffUrl(Deal deal){
//        //TODO: will call the monlinks API
//        //TODO: call link affiliator of prosperent
//        //currently will use default properent
////		String PROSP_URL_TEMPLATE = "http://prosperent.com/api/linkaffiliator/redirect?apiKey=%s&location=%s&url=%s";
////		String pubId = "34e313e356738fa5d0f03cd70db9a8ea";
////		return String.format(PROSP_URL_TEMPLATE, pubId ,URLEncoder.encode("http://facebook.com"), URLEncoder.encode(merchantUrl));
//
//        String retAffUrl = null;
//
//        //if affiliate url already exist take it
//        if(deal.getAffUrl()!= null && !deal.getAffUrl().isEmpty()){
//            //The affiliate url already exist. Don't do anything, just return the affiliate url
//            retAffUrl = deal.getAffUrl();
//        }else{
//            String deepLink = deal.getDeepLink();
//            if(deepLink == null){
//                deepLink = deal.getMerchant().getHomePageUrl();
//            }
//
//            retAffUrl = getAffUrlViglink(deepLink);//getAffUrlProsperent(deepLink);
//
//        }
//
//        log.debug("retAffUrl="+ retAffUrl);
//        return retAffUrl;
//
//    }

//    private String getAffUrlViglink(String deepLink) {
//        //VIGLINK
//        String VIG_URL_TEMPLATE = "http://redirect.viglink.com?key=%s&u=%s";
//        String pubId = "e705fdd58001fe5ef00dbda2d47ad71a";//"e705fdd58001fe5ef00dbda2d47ad71a";
//        return String.format(VIG_URL_TEMPLATE, pubId , URLEncoder.encode(deepLink));
//
//    }
//
//    private String getAffUrlProsperent(String deepLink) {
//        String PROSP_GET_LINK_URL_TEMPLATE = "http://prosperent.com/api/linkaffiliator/url?apiKey=%s&location=%s&url=%s";
//        String pubId = "34e313e356738fa5d0f03cd70db9a8ea";
//        String getLinkUrl = String.format(PROSP_GET_LINK_URL_TEMPLATE, pubId ,URLEncoder.encode("http://facebook.com"), URLEncoder.encode(deepLink));
//        log.debug("GET LINK from:"+ getLinkUrl);
//
//        String prosAffUrl = getAffUrlFromPros(getLinkUrl);
//        log.debug("prosAffUrl="+ prosAffUrl);
//        String retUrl = null;
//        if(prosAffUrl != null){
//            retUrl  = prosAffUrl;
//        }
//
//        if(!retUrl.contains("prosperent")){
//            log.warn("This merchant website is not affiliated by prosperent: "+ retUrl);
//        }
//
//        log.debug("retUrl="+ retUrl);
//        return retUrl;
//    }

}
