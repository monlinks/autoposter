package com.monlinks.dealsposter.dealsposterservice.postsender;

import com.monlinks.dealsposter.dealsposterservice.textBuilder.PostTextBuilder;
import com.monlinks.dealsposter.model.publisher.PostingDestination;
import com.monlinks.dealsposter.model.publisher.PostingDestination.DestinationTypeEnum;
import com.monlinks.dealsposter.model.publisher.PostingUser;
import com.monlinks.dealsposter.utils.AffiliationUrlNotValidException;
import com.monlinks.dealsposter.utils.UrlRelatedHelper;
import facebook4j.*;
import facebook4j.auth.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

import javax.swing.*;
import java.awt.Image;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

@Configurable
public class FacebookPostSender extends PostSender {
	
	Facebook facebook = null;
	String appId = "192777631205213";
	String appSecret = "d31438f675dae0ed349bd577e73613b7";
	String commaSeparetedPermissions = "user_managed_groups, publish_actions, public_profile";
	//TODO: take from configuration
	//permanent access token for page 
	//String accessToken = "EAASSZAhpvYAMBAO2WUyJlt5ZBzCbF6EcZANs58X5OQxyV2eG2ZBDAAVWYE76D8xTx6zZCf2ZAQCLDaXSI3IRx4uzY0ygHFhb0mdnkarbmpiPDYzKQRFZC75824dxGPxGvYgJ5IKmHB4kOiWKFaAht1iqB37voxaIZCUZD";//appId+"|"+appSecret;//lior:"EAASSZAhpvYAMBABDJUf1dlomiN10brErAec74n3QeZAsNTuWtTPIXIuwk1cSYacFwYKQk7RE21RwrvX7RIHsC37JtK4SO1QYxO2qUZCyqhQuMICYr5MSjjePTZBZAFYC49bex9ZAhH6Cgr8DfcHGbM";//appId+"|"+appSecret;//"EAASSZAhpvYAMBABrTUqfZABIAEY9gLxJYORUEJ19mmvIvt7be8h2pvFZBfSldos8VNkjPsqYpkCtWpQasZAr6g5zR91Ic5YYLvMBiq0oJBIgln7SHFXoJJKNN1isiwWFY7jQgAxmvNNkjSsB3Avm";
	//String accessToken = "EAACvVIo6h10BAEuZCbg8aVlYoj8gE2XiC6QZBU7ShyNLIZAKSqTeQzP0blURZAGGpeWmV9tzjZBBQoKw0wshegjnQHS7sJWRKvPkrDmNr4XZBoymzAQbAfMEzisfUhlYBeRb2UKQ0ZBpIqj6mDUVgT9AWJBPt0HOY4ZD";//appId+"|"+appSecret;//
	String DammyfacebookGroupId = "1821915298095868";//fashion groupid - just for temp post
	private ResourceBundle config = ResourceBundle.getBundle("application",Locale.ROOT);

	private boolean isRunningInTestMode = false;
	private PostingUser postingUser;
	
	private static final Logger log = LoggerFactory.getLogger(FacebookPostSender.class);
	
			
		
	public FacebookPostSender(PostingUser postingUser) throws Exception {
		super();
		
		log.debug("Build Facebook User:"+ postingUser.getPublisherName());
		facebook = new FacebookFactory().getInstance();
		
		facebook.setOAuthAppId(appId, appSecret);
		facebook.setOAuthPermissions(commaSeparetedPermissions);
		log.debug("SET accessToken="+ postingUser.getUserAuthKey());
		AccessToken currentAccessToken = new AccessToken( postingUser.getUserAuthKey(), null);
		facebook.setOAuthAccessToken(currentAccessToken);
		
		int accessTokenDaysLeft =  calcAccessTokenDaysLeft();
		log.debug("accessTokenDaysLeft="+accessTokenDaysLeft);
		
		if(accessTokenDaysLeft < 1){
			log.error("Access Token is Expiered. Will not publish to facebook. accessToken="+postingUser.getUserAuthKey());
			throw new Exception("Access Token is Expiered. Will not publish to facebook.");
		}else if(accessTokenDaysLeft < 7){
			log.info("RENEW access token!. Going to be expiered in "+ accessTokenDaysLeft +" days");
		}
		
		//reloadAccessToken();//extend from long term access token is not extending the expiration date!
		
		isRunningInTestMode = Boolean.parseBoolean(config.getString("publishDeals.test.mode"));
				
		this.postingUser = postingUser;		
	}



	private int calcAccessTokenDaysLeft()  {
		long expirationInDays = 0;
		try{
		AccessToken accessTokenInfo = facebook.getOAuthAccessTokenInfo();
		log.debug("accessTokenInfo="+accessTokenInfo);
		
		long expirationInSeconds = accessTokenInfo.getExpires();
		log.debug("expirationInSeconds="+ expirationInSeconds);
		expirationInDays = expirationInSeconds / 60 / 60 / 24;
		log.debug("expirationInDays="+ expirationInDays);
		}catch(FacebookException fe){
			log.error(fe.getMessage(),fe);
		}
		
		return (int) expirationInDays;
	}



//	private void reloadAccessToken(){
//		try{
//			log.debug("Try to reload Access Token with: appId="+ appId +",appSecret="+appSecret);
//			facebook.setOAuthAppId(appId, appSecret);
//			facebook.setOAuthPermissions(commaSeparetedPermissions);
//	
//			log.debug("SET accessToken="+accessToken);
//			facebook.setOAuthAccessToken(new AccessToken(accessToken, null));
//			
//			AccessToken accessTokenInfo = facebook.getOAuthAccessTokenInfo();
//			log.debug("accessTokenInfo="+accessTokenInfo);
//			
//			long expirationInSeconds = accessTokenInfo.getExpires();
//			log.debug("expirationInSeconds="+ expirationInSeconds);
//			long expirationInDays = expirationInSeconds / 60 / 60 / 24;
//			log.debug("expirationInDays="+ expirationInDays);
//			
//			AccessToken extendedAccessToken = facebook.extendTokenExpiration();
//			accessToken = extendedAccessToken.getToken();
//			facebook.setOAuthAccessToken(extendedAccessToken);
//			log.debug("extendedAccessToken="+extendedAccessToken);
//			
//			long expirationInSeconds2 = extendedAccessToken.getExpires();
//			log.debug("expirationInSeconds2="+ expirationInSeconds2);
//			long expirationInDays2 = expirationInSeconds2 / 60 / 60 / 24;
//			log.debug("expirationInDays2="+ expirationInDays2);
//			
//		
////			AccessToken accessToken = facebook.getOAuthAppAccessToken();
////			log.debug("GOT accessToken="+accessToken);
////			facebook.setOAuthAccessToken(accessToken);
//		} catch (FacebookException e) {
//			log.error("Problem in getting new access token.",e);
//		}
//
//	}
	
	public String postTheDeal(IPostData postDataObj,PostingDestination postingDestination) throws MalformedURLException, FacebookException, AffiliationUrlNotValidException {
		return postAsFeed(postDataObj,postingDestination);
		//postAsLink(postDataObj);	
	}
	
	private String postAsFeed(IPostData postDataObj, PostingDestination postingDestination) throws MalformedURLException, FacebookException, AffiliationUrlNotValidException {
		
		PostUpdate couponPost = createCouponPost((DealPostData) postDataObj,postingDestination);				
		
		String postId = postToFacebook(couponPost,postingDestination);
		
		//test the the published image is good
		if(isRunningInTestMode){
			log.info("Running in test mode: will not check the final post image size");
		}else{
			log.debug("Will check the final post image size");
			Post postDataFromFB = facebook.getPost(postId,new Reading().fields("picture").addParameter("type", "large"));
			log.debug("Returned post data:"+ postDataFromFB);
			
			URL finalPostUrl = postDataFromFB.getPicture();
			log.debug("Post image URL="+ postDataFromFB.getPicture());
			
			//if for some reason the final post image is too small, delete the post and replace it with random image from category
			if(checkIsImageTooSmall(finalPostUrl)){
				log.debug("Final post image is too small, replace with random image");
				String replacedPostId = replacePostWithRandomImage(postId,couponPost,postingDestination);
				log.debug("replacedPostId="+ replacedPostId);
			}
		}

		return postId;
				
	}
	
	private String postToFacebook(PostUpdate couponPost, PostingDestination postingDestination) throws FacebookException {
		String postId = "12345678790";
		log.debug("Going to POST:"+ printPostData(couponPost) );
		log.debug("To destination:"+ postingDestination );
		DestinationTypeEnum detinationType = postingDestination.getDestinationType();
		String destinationId= postingDestination.getDestinationId();
				
		if(detinationType.equals(DestinationTypeEnum.GROUP)){
			//******************* POST TO FACEBOOK ***********/
			if(isRunningInTestMode){
				log.info("Running in test mode: will not post to Facebook. will use fake postId="+ postId);
			}else{
				log.debug("Will post now to facebook group");
				//https://developers.facebook.com/docs/apps/changelog/
				/*
				Form 18/7/2017:
				The following fields are deprecated for edges and dialogs that allow attaching links to posts:
picture
name
caption
thumbnail
description
The edges and dialogs for which these are deprecated include POST /{user-id}/feed, /{page-id}/feed, /{event-id}/feed, and /{group-id}/feed as well as the share and feed dialogs.
				 */
				postId = facebook.postGroupFeed(destinationId, couponPost);//destinationId => groupId
			}
			
			//************************************************/
			
			log.debug("PostGroupFeed to facebook was successfull. postID="+postId);
			
		}else{
			//******************* POST TO FACEBOOK PAGE***********/
			if(isRunningInTestMode){
				log.info("Running in test mode: will not post to Facebook. will use fake postId="+ postId);
			}else{
				log.debug("Will post now to facebook page");
				postId = facebook.postFeed(destinationId, couponPost);//Post to  page as the page
			}

			//************************************************/

			log.debug("postFeed to facebook was successfull. postID="+postId);
		}
		
		return postId;
	}

	private String replacePostWithRandomImage(String postIdToDelete, PostUpdate couponPost, PostingDestination postingDestination) {
		String postId = null;
//		try {
//			//delete the post
//			facebook.deletePost(postIdToDelete);
//			log.debug("Post was deleted. postIdToDelete ="+postIdToDelete);
//			
//			URL randomImage = getRandomImageByCategory();
//			
//			couponPost.setPicture(randomImage);
//			
//			postId = postToFacebook(couponPost,postingDestination);
//			
//		} catch (FacebookException|IOException e) {
//			log.error("Problem to replacePostWithRandomImage",e);
//		} 
		
		return postId;
		
	}

	private URL getCorrectLinkImage(DealPostData postDataObj) throws MalformedURLException, FacebookException, AffiliationUrlNotValidException {
		URL retImageUrl = null;
		String domainName = postDataObj.getDomainName();
		
		//if deal have post image
		if(postDataObj.getImgUrl()!= null && !postDataObj.getImgUrl().isEmpty()){
			retImageUrl =  new URL(postDataObj.getImgUrl()); // might be already an image from the Deal itself
			log.debug("Image from the original deal:"+ retImageUrl);
		}
		
		//if deal have no image try to get it from the deeplink or affiliate link
		if(retImageUrl==null){
			//try to get the correct image from the affiliate url
			String deepLink = postDataObj.getDeepLink();

			if(deepLink != null){
				log.debug("Get image for deepLink="+ deepLink);
				retImageUrl = getImageFromFakePostURL(deepLink);
			}else{
				//should not get here in most cases becouse deeplink is updated at com.monlinks.dealsposter.dealsposterservice.DealsPublisherJobExecutor.checkDealPostDataValidity()
				log.debug("Get image for affUrl="+ postDataObj.getAffUrl());
				retImageUrl = getFinalLinkImgUrl(postDataObj.getAffUrl());
			}
			log.debug("retImageUrl="+ retImageUrl);
		}
		
		
		if(checkIsImageTooSmall(retImageUrl)){
			retImageUrl = getImageFromFakePost(domainName);
			log.debug("Image from fake post:"+ retImageUrl);
			
			if(checkIsImageTooSmall(retImageUrl))	{
				retImageUrl = getAlternativeImage(postDataObj);//logo or random image
			}
			
		}	
				
		log.debug("FINAL image to be used in post:"+ retImageUrl);	
		
		return retImageUrl;
	}
	
	private URL getFinalLinkImgUrl(String affLink) throws AffiliationUrlNotValidException, MalformedURLException, FacebookException {
		URL retImageUrl = null;

			String finalAffUrl = UrlRelatedHelper.calcFinalAffUrl(affLink);
			log.debug("finalAffUrl="+finalAffUrl);
			retImageUrl = null;//getImageFromFakePostURL(finalAffUrl) ;//ImageExtractor.extractImageUrl(finalAffUrl);

		
		log.debug("retImageUrl="+retImageUrl);
		return retImageUrl;
	}



	private URL getAlternativeImage(DealPostData postDataObj){
		URL retImageUrl = null;
		try {
			//Try to take a logo
			String logoUrl = "http://logo.clearbit.com/"+postDataObj.getDomainName()+"?size=400";
			log.debug("Try logoUrl="+ logoUrl);
			retImageUrl = new URL(logoUrl);

//			if(checkIsImageTooSmall(retImageUrl)){
//				retImageUrl = getRandomImageByCategory();
//			}

		} catch (IOException e) {
			log.error("Problem in getting image from URL:"+ retImageUrl,e);
		}

		log.debug("return: retImageUrl="+ retImageUrl);
		return retImageUrl;
	}

	
	


	
	private URL getImageFromFakePostURL(String url) throws MalformedURLException, FacebookException {
		URL retImageUrl = null;
		log.debug("Going to post a FAKE post for "+ url);
		//try {	
			//First post a link in private 
			
			String linkPostId;

			try{

				linkPostId = facebook.postLink(new URL(url));//so now one will see it
			}catch(FacebookException|IllegalStateException e){
				log.error("Failed in dummy postLink for getting the share image",e);
				throw e;
			}

			log.debug("Finish post a link to "+url+".linkPostId="+linkPostId);

			//get the image
			Post postDataFromFB = facebook.getPost(linkPostId,new Reading().fields("picture").addParameter("type", "large"));
			log.debug("Returned post data:"+ postDataFromFB);
			retImageUrl = postDataFromFB.getPicture();
			log.debug("retImageUrl="+ retImageUrl);

			//delete the post
			facebook.deletePost(linkPostId);
			log.debug("Post was deleted. linkPostId ="+linkPostId);

//		} catch (FacebookException e) {
//			log.warn("Fail to post a FAKE post with link: http://"+domainName,e );
//		}
		return retImageUrl;
	}
	
	private URL getImageFromFakePost(String domainName) throws MalformedURLException, FacebookException {
		URL retImageUrl = null;
		log.debug("Going to post a FAKE post for "+ domainName);
		//try {	
			//First post a link in private 
			String url = "http://"+domainName;
			retImageUrl = null;//getImageFromFakePostURL(url);
			return retImageUrl;
	}

//	private String checkImageAndUpdatePost(PostUpdate couponPost, String postId, String imgUrl) throws MalformedURLException, FacebookException{
//		if(imgUrl==null || imgUrl.isEmpty()) return postId;
//		
//		URL picture = new URL(imgUrl);
//		String retPostId = postId;
//		if(checkIsImageTooSmall(picture)){
//			log.debug("Logo image is too small, will not replace the post. imge logo url="+ imgUrl);
//		}else{
//			log.debug("Logo image size is good enough, going to replace the post. imge logo url="+ imgUrl);
//			
//			retPostId = updatePostWithNewImage(postId,couponPost,picture);
//			
//		}
//		
//		return retPostId;
//	}
	
	
	

//	private String updatePostWithNewImage(String postId, PostUpdate couponPost, URL newPicture) throws FacebookException {
//		//update the post in facebook
//		//delete current post
//		facebook.deletePost(postId);
//		log.debug("Post was deleted. postID="+postId);
//		
//		log.debug("update post data with new picture:" + newPicture);
//		couponPost.setPicture(newPicture);
//		
//		log.debug("Going to rebuplish the post with the updated picture");
//		String postId2 = facebook.postGroupFeed(facebookGroupId, couponPost);
//		log.debug("PostGroupFeed to facebook was successfull. postID="+postId2);
//		
//		
//		boolean isNewPostShowImage = isPostShowImage(postId2);
//		log.debug("isNewPostShowImage="+isNewPostShowImage);
//		
//		return postId2;
//		
//	}

//	private boolean isPostShowImage(String postId) throws FacebookException {
//		boolean retAns = false;
//		Post postDataFromFB = facebook.getPost(postId,new Reading().fields("picture"));
//		log.debug("Returned post data:"+ postDataFromFB);
//		URL pictureInPost = postDataFromFB.getPicture();
//		if(pictureInPost!=null){
//			//BAD: https://fbexternal-a.akamaihd.net/safe_image.php?d=AQDmHXRkmA-yOM-m&w=130&h=130&url=http%3A%2F%2Fassets.peaceloveworld.com%2Fskin%2Ffrontend%2Fplw%2Fdefault%2Fimages%2Fplw-logo.png&cfs=1&_nc_hash=AQDRjiSLLBlreBto
//			//GOOD: https://fbexternal-a.akamaihd.net/safe_image.php?d=AQByLx7OoHq8gtVv&w=476&h=249&url=http%3A%2F%2Fmediaus.topman.com%2Fwcsstore%2FConsumerDirectStorefrontAssetStore%2Fimages%2Fcolors%2Fcolor9%2Fcms%2Fpages%2Fstatic%2Fstatic-0000097180%2Fimages%2FWK16-best-of-street-style-UK-US-AUS.jpg&cfs=1&upscale=1&sx=17&sy=0&sw=956&sh=500&_nc_hash=AQD4_fIONaJ-XanH
//			boolean isImageTooSmall = checkIsImageTooSmall(pictureInPost);
//			log.debug("isImageTooSmall="+ isImageTooSmall);
//			if(!isImageTooSmall){
//				retAns = true;
//			}
//		}
//		
//		log.debug("isPostShowImage ret="+ retAns);
//		return retAns;
//	}

	private boolean checkIsImageTooSmall(URL imageUrlToTest) {
		
		boolean retAns = false;
		
		if(imageUrlToTest == null || "".equals(imageUrlToTest)){
			log.debug("imageUrlToTest is null. return checkIsImageTooSmall=true");
			return true;
		}
	
		log.debug("Going to check dimentions of image from post:"+ imageUrlToTest);
		Image image = new ImageIcon(imageUrlToTest).getImage();
		if(image==null){
			log.debug("The url "+ imageUrlToTest +" return no image. return checkIsImageTooSmall=true");
			return true;//no image means its too small
		}
		
		int imgWidth = image.getWidth(null);
		int imgHeight = image.getHeight(null);
		log.debug("imgWidth="+ imgWidth +" , imgHeight="+imgHeight);
		
		log.debug("Less then 130 will be considered as small");
		retAns = (imgWidth<130 || imgHeight<130);
		if(log.isDebugEnabled()){
			if(retAns){
				log.debug("The image "+imageUrlToTest +", is too SMALL.");
			}else{
				log.debug("The image "+imageUrlToTest +", is in GOOD size.");
			}
		}
		
	
		return retAns;
	}

	private String printPostData(PostUpdate couponPost) {
		 return "### PostUpdate{" +"\n" +
	                "message='" + couponPost.getMessage() + "\'\n" +"\n" +
	                "link=" + couponPost.getLink() +"\n" +
	                "picture=" + couponPost.getPicture() +"\n" +
	                "name='" + couponPost.getName() + "\'\n" +
	                "caption='" + couponPost.getCaption() + "\'\n" +
	                "description='" + couponPost.getDescription() + "\'\n" +"\n" +
	                ", actions=" + couponPost.getActions() +
	                ", place='" + couponPost.getPlace() + '\'' +
	                ", tags='" + couponPost.getTags() + '\'' +
	                ", privacy=" + couponPost.getPrivacy() +
	                ", objectAttachment='" + couponPost.getObjectAttachment() + '\'' +
	                ", targeting=" + couponPost.getTargeting() +
	                ", published=" + couponPost.getPublished() +
	                ", scheduledPublishTime=" + couponPost.getScheduledPublishTime() +
	                '}';
	}

//	private void postAsLink(IPostData postDataObj) throws MalformedURLException, FacebookException {
//		String affUrl = postDataObj.getAffUrl();
//		log.debug("affUrl="+affUrl);
//		String msgTosend =  buildLinkMessage(postDataObj);
//		log.debug("msgTosend="+msgTosend);
//		//facebook.postGroupLink(facebookGroupId, new URL(affUrl), msgTosend);
//	}
//
//
//	private String buildLinkMessage(IPostData postDataObj){
//		String firstLine = "קוד קופון לאתר "+ makeItNotLinkable(postDataObj.getMerchantName())+" , העתיקו את קוד הקופון "+postDataObj.getCode()+" והכנסו מכאן לאתר.";
//		String secondLine =  postDataObj.getTitle();
//		String thirdLine = "קוד קופון: "+postDataObj.getCode()+" |  תוקף: "+postDataObj.getStartDate()+"-"+postDataObj.getEndDate();
//		String msgToPost = firstLine+"\r\n "+secondLine+"\r\n "+thirdLine;
//		log.debug("msgToPost = "+ msgToPost);
//		return msgToPost;
//	}
//	
private PostUpdate createCouponPost(DealPostData postDataObj, PostingDestination postingDestination) throws MalformedURLException, FacebookException, AffiliationUrlNotValidException {
	PostTextBuilder postTextBuilder = new PostTextBuilder(postDataObj,postingDestination.getPostLang());
	PostUpdate post = new PostUpdate(new URL(postDataObj.getAffUrl()))
			.message(postTextBuilder.getMessageText());
	return post;
}



	public boolean isRunningInTestMode() {
		return isRunningInTestMode;
	}

	public void setRunningInTestMode(boolean runningInTestMode) {
		isRunningInTestMode = runningInTestMode;
	}

	
	

}
