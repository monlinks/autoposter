package com.monlinks.dealsposter.dealsposterservice.postsender;

import com.monlinks.dealsposter.model.publisher.PostingDestination;
import com.monlinks.dealsposter.utils.AffiliationUrlNotValidException;
import facebook4j.FacebookException;

import java.net.MalformedURLException;

public interface IPostSender {

	String postTheDeal(IPostData postDataObj, PostingDestination postingDestination) throws MalformedURLException, FacebookException, AffiliationUrlNotValidException;
}
