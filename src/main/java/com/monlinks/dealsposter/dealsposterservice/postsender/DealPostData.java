package com.monlinks.dealsposter.dealsposterservice.postsender;

import com.monlinks.dealsposter.model.Deal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class DealPostData implements IPostData{
	
	private static final Logger log = LoggerFactory.getLogger(DealPostData.class);

	private String startDate;
	private String endDate;
	private int daysToExpiration;
	private String title;
	private String domainName;
	private String merchantName;
	private String imgUrl = null;
    private String code;
	private String affUrl;
	private String category;
	private String description;
	private String deepLink;
	
	
	
	
	public DealPostData(Deal deal) {


		try {
			if(deal != null){
				title = deal.getTitle();
				setDescription(deal.getDescription());
				domainName = extractDomain(deal.getMerchant().getHomePageUrl());
				merchantName = deal.getMerchant().getName();
				setCode(deal.getCode());
				startDate = formatDate(deal.getStartDate());
				endDate = formatDate(deal.getEndDate());
				imgUrl = deal.getDealImageUrl();
				category = null;//deal.getCategory();
				deepLink = deal.getDeepLink();
				if(deepLink == null){
					deepLink = deal.getMerchant().getHomePageUrl();
				}
			}
		} catch (Exception e) {
			log.error("Problem creating DealPostData",e );
		}

	}

	








	private String getAffUrlFromPros(String getLinkUrl) {
		String retUrl = null;
		InputStream is = null;
		try {
			URL url = new URL(getLinkUrl);
			URLConnection conn = url.openConnection();
			is = conn.getInputStream();
			
			Scanner s = new Scanner(is).useDelimiter("\\A");
			retUrl = s.hasNext() ? s.next() : "";
			s.close();
			
		} catch (Exception e) {
			log.error("Problem while getting HTTP from:"+getLinkUrl , e);
		}finally {
			if(is != null){
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		log.debug("retUrl="+ retUrl);
		return retUrl;
	}


	private String extractDomain(String fullUrl) {
		try {
			URI uri = new URI(fullUrl);
			String domain = uri.getHost();
			return domain.startsWith("www.") ? domain.substring(4) : domain;
		} catch (URISyntaxException e) {
			log.error("Problem in url="+ fullUrl,e);
		}
		
		return fullUrl;
	}


	private String formatDate(String dateStr) {
		String retDate = null;
				
		try {
			 // *** note that it's "yyyy-MM-dd hh:mm:ss" not "yyyy-mm-dd hh:mm:ss"  
		        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		        Date date = dt.parse(dateStr);

		        // *** same for the format String below
		        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
		        retDate = dt1.format(date);
		} catch (Exception e) {
			log.error("Date cannot be formated. dateStr="+dateStr);
		} 
				
		return retDate;
	}


	public String getImgUrl() {
		return imgUrl;
	}



	public String getStartDate() {
		return startDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public String getTitle() {
		return title;
	}


	public String getDomainName() {
		return domainName;
	}


	public String getMerchantName() {
		return merchantName;
	}


	


	public String getAffUrl() {
		return affUrl;
	}


	@Override
	public String toString() {
		return "OfferPostData [ merchantName=" + merchantName + ",startDate=" + startDate + ", endDate=" + endDate + ", title="
				+ title + ", domainName=" + domainName + ", imgUrl=" + imgUrl
				+ ", affUrl=" + affUrl + "]";
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}


	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}


	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	public void setAffUrl(String affUrl) {
		this.affUrl = affUrl;
	}




	public void setCode(String code) {
		this.code = code;
	}


	public String getCode() {
		return code;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getDaysToExpiration() {
		return daysToExpiration;
	}


	public void setDaysToExpiration(int daysToExpiration) {
		this.daysToExpiration = daysToExpiration;
	}





	public String getDeepLink() {
		return deepLink;
	}





	public void setDeepLink(String deepLink) {
		this.deepLink = deepLink;
	}


	
	
}
