package com.monlinks.dealsposter.dao;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.LimitOperation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.collector.Collector;
import com.monlinks.dealsposter.model.publisher.Publisher;

public interface CollectorsRepository extends MongoRepository<Collector, String> {

	public List<Collector> findByName(String name);

	public List<Collector> findByEnabled(boolean enabled);

    
}