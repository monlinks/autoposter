package com.monlinks.dealsposter.dao;

import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.publisher.PublishAction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface DealsRepository extends MongoRepository<Deal, String> {

    //public Deal findByPostedDate(String postDate);
   // public List<Deal> findByPostedDateAndCategory(String postDate, String category, Pageable pageable);
   // public List<Deal> findByPostedDateAndPosterId(String postDate, String posterId, Pageable pageable);

   // public List<Deal> findByPostedDateAndPosterId(String postDate, String posterId, Sort sort);
    
   // public List<Deal> deleteByPostedDate(String postDate);

	public List<Deal> deleteByAffUrl(String string);
	
	@Query("{'?0':{$regex:?1}}")
	public List<Deal> findByRegExp(String attName, String regex);
	
	@Query("{ 'category': '?0', 'audienceLocations': '?1', 'postedTo.destinationId': { '$ne': '?2' } , 'skipReason': null}")
	public List<Deal> findUnpublishedDealsForThisPublishJobByCategory(String category, String audienceLocation, String destinationId, Pageable pagingData);

//	{
//		"merchant.homePageUrl": {
//		"$in": [
//		{
//			"$regex": "allsole.com"
//		},
//		{
//			"$regex": "columbia.com"
//		}
//        ]
//	}
//	}

//	{
//		"merchant.homePageUrl": {
//		"$regex": "allsole.com|columbia.com"
//	}
//	}
//
	@Query("{ 'merchant.homePageUrl': {'$regex':'?0'}, 'audienceLocations': '?1', 'postedTo.destinationId': { '$ne': '?2' }, 'skipReason': null}")
	public List<Deal> findUnpublishedDealsForThisPublishJobByList(String merchantsList, String audienceLocation, String destinationId, Pageable pagingData);

	@Query("{'postedTo': {'$size': 0 }	, 'skipReason': null}")
	public List<Deal> findAllUnpublishedDeals(Sort sortData);

	@Query("{'postedTo': {'$size': 0 },'skipReason': { '$ne': null }}")
	public List<Deal> findAllUnpublishedCleanedDeals();

	@Query("{'postedTo': {'$size': 0 }	, 'category': '?0'}")
	public List<Deal> findUnpublishedDealsByCategory(String category,Sort sortData);

//	{ 
//	    "endDate": { 
//	        "$lt": "2017-07-20 03:59:59" 
//	    } 
//	}
	@Query("{'endDate': {'$lt': '?0' }, 'skipReason': null}")
	public List<Deal> findDealsExpiersBefore(String dateTime);
	
	@Query("{'postedTo': {'$gt': []}	}")
	public List<Deal> findAllPublishedDeals();

	@Query("{'postedTo.actionResult': 'FAILED'}")
	public List<Deal> findAllFailedDeals();
	
	

	public List<Deal> findUnpublishedDealsByMerchantName(String merchantName,Sort sortData);

	public List<Deal> findByPostedTo(List<PublishAction> publishActions);

    


    //public List<Deal> findByPostedByPosterId(String postedBy, String destinationId, Pageable pagingData);
	
	
	//postDate=null , 

	//@Query("{ 'category': 'TRAVEL', 'audienceLocation': 'IL', 'merchant.location': 'ALL',  'postedTo.destinationId': { '$ne': 'G1' }}")
}