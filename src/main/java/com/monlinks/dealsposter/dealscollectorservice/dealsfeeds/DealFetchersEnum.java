package com.monlinks.dealsposter.dealscollectorservice.dealsfeeds;

public enum DealFetchersEnum {
	
	GROUPON_IL(new GrouponILDealsFetcher()),
	GROUPON_US(new GrouponUSDealsFetcher()),
	ICODES(new ICodesDealsFetcher()),
	AMAZON(new AmazonDealsFetcher()),
	SKIMLINKS(new SkimlinksDealsFetcher());

	//YIELD(new YieldkitDealsFetcher()),
	//AMAZON,EBAY,ALI,CJ,AWIN
	
	IDealsFetcher dealsFetcher = null;
	DealFetchersEnum(IDealsFetcher dealsFetcher){
		this.dealsFetcher = dealsFetcher;
	}
	public IDealsFetcher getDealsFetcher() {
		return dealsFetcher;
	}
	public void setDealsFetcher(IDealsFetcher dealsFetcher) {
		this.dealsFetcher = dealsFetcher;
	}

}
