package com.monlinks.dealsposter.dealscollectorservice.dealsfeeds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.Merchant;
import com.monlinks.dealsposter.model.MerchantLocation;
import com.monlinks.dealsposter.model.common.AudienceLocationEnum;
import com.monlinks.dealsposter.model.publisher.PublishAction;
import com.monlinks.dealsposter.utils.ArrayUtils;

public class YieldkitDealsFetcher extends AbsDealsFetcher {
	private static Logger log = LogManager.getLogger(YieldkitDealsFetcher.class.getName());
	//Affiliate Id	202128 ,Media Id 515
	//Merchant commission: http://api.yieldkit.com/v1/advertiser?api_key=6817b3630fcc987430aebc097413a7b0&api_secret=7815470cae0ecb86771f8d3a6f44426d&site_id=2e4c8f098e864b52a43eae165498314c&q=toy&page_size=10&page=1&format=json
	//http://api.yieldkit.com/v1/incentive?api_key=c5c2398597a6adcd9b149ad745f207f4&api_secret=74607007cdb6b0db4b3219c8adee3e09&site_id=50880c31e4b0f12859e3c14b&page_size=50&page=1&format=json&q=booking&country=US
	String dealFetchUrlTemplate = "http://api.yieldkit.com/v1/incentive?api_key=6817b3630fcc987430aebc097413a7b0&api_secret=7815470cae0ecb86771f8d3a6f44426d&site_id=2e4c8f098e864b52a43eae165498314c&page_size=50&page=1&format=json&q=%s&country=%s";
	
	
	@Override
	public List<Deal> fetchDealsByCategory(String website, MerchantLocation location) {
		log.error("NOT IMPLEMENTED");
		return null;
	}
	
//	@Override
//	public List<Deal> fetchDealsByList(String merchantsList, MerchantLocation merchantLocation) {		
//		String formatedUrl = String.format(dealFetchUrlTemplate, website);
//		JsonArray grouponsDeals = fetchDealsArrayFromUrl(formatedUrl);
//		List<Deal> retDeals = convertJsonToDeals(grouponsDeals,location,category);
//		return retDeals;
//		
//	}
	
	public List<Deal> fetchDealsByList(String merchantsList, MerchantLocation merchantLocation) {
		JsonArray retArr = null;
		List<Deal> retDeals = null;
		String merchantNamesListStr = merchantsList;
		String[] merchantNamesListArr = merchantNamesListStr.split(",");
		log.debug("Go over merchant list:" + Arrays.toString(merchantNamesListArr));

		for (String merchantName : merchantNamesListArr) {
			log.debug("@@@@@@@@@ Going to fetch deals for : " + merchantName);
			if(merchantName.trim().contains(" ")){
				log.debug("Skip this website becouse have spaces:"+ merchantName.trim());
				continue;
			}
			
			JsonArray thisMerchantDeals = null;
			if (merchantLocation.equals(MerchantLocation.ALL)) {
				JsonArray arr1 = fetchDeals(merchantName, MerchantLocation.US);
				JsonArray arr2 = fetchDeals(merchantName, MerchantLocation.UK);			
				thisMerchantDeals = ArrayUtils.calcCorrectArr(arr1, arr2);
			} else {
				thisMerchantDeals = fetchDeals(merchantName, merchantLocation);	
			}

			int numberOfDeals = (thisMerchantDeals == null ? 0 : thisMerchantDeals.size());
			if(numberOfDeals>0){
				log.debug("@@@@@@@@@ Found " + numberOfDeals + " deals for : " + merchantName);
			}else{
				log.debug("@@@@@@@@@ NO deals were found for merchant : " + merchantName);
			}

			retArr = ArrayUtils.calcCorrectArr(retArr, thisMerchantDeals);
		}

		retDeals = convertJsonToDeals(retArr, merchantLocation);
		return retDeals;
	}
	
	private JsonArray fetchDeals(String merchantName, MerchantLocation merchantLocation) {
		String formatedUrl = String.format(dealFetchUrlTemplate, merchantName,merchantLocation.name());
		JsonArray yieldKitDeals = fetchDealsArrayFromUrl(formatedUrl);
		return yieldKitDeals;
	}


//	id: "019d061a9e7641d18b15e518810499e0",
//	advertiserId: "18141aee1d2a4eb78ca4a29754871381",
//	advertiser: "Booking.com Room Sales US",
//	advertiserDomain: "booking.com",
//	description: "Save Up to 50% in Munich! No Booking Fees Ever!",
//	name: "Booking.com Room Sales US",
//	code: "",
//	validFrom: "2017-02-14",
//	validTo: "2018-12-31",
//	trackingLink: "http://r.srvtrck.com/v1/redirect?type=incentive&id=019d061a9e7641d18b15e518810499e0&api_key=c5c2398597a6adcd9b149ad745f207f4&site_id=50880c31e4b0f12859e3c14b&dch=feed&ad_t=coupon"
	protected Deal createDeal(JsonObject jsonDeal, MerchantLocation location2)  {
		String extId = getStringFromJsonAtt(jsonDeal,"id"); 
		String merchantName = getStringFromJsonAtt(jsonDeal,"advertiser"); 
		String merchantdomain = "http://"+getStringFromJsonAtt(jsonDeal,"advertiserDomain"); 
		MerchantLocation location = location2;
		String imageUrl = null;
		Merchant merchant = new Merchant(merchantName, merchantdomain, location, imageUrl);

		String description = getStringFromJsonAtt(jsonDeal,"description"); 
		String code = getStringFromJsonAtt(jsonDeal,"code"); 
		String discount = null;
		String dealImageUrl = null;
		String postedDate = null;// not posted yet
		String startDate = getStringFromJsonAtt(jsonDeal,"validFrom")+" 00:00:00";// Add time to fix the format for the publisher
		String endDate = getStringFromJsonAtt(jsonDeal,"validTo")+" 23:59:59";
		String title = getStringFromJsonAtt(jsonDeal,"name"); 
		
		String affUrl = getStringFromJsonAtt(jsonDeal,"trackingLink");
		String collectDate =LocalDateTime.now().toString();
		String dealSource= "api.yieldkit.com";
		ArrayList<PublishAction> postedBy = new ArrayList<PublishAction>();
				
		ArrayList<AudienceLocationEnum> audianceLocations = new ArrayList<AudienceLocationEnum>();
		audianceLocations.add(AudienceLocationEnum.IL);
		audianceLocations.add(AudienceLocationEnum.US);
		
		Deal dealToInsert = new Deal(extId, merchant, description, code, discount, dealImageUrl, postedDate, startDate,	endDate, title,affUrl, collectDate, dealSource , null,null,postedBy,audianceLocations );
		return dealToInsert;
	}
	
//private String selectImage(JsonObject jsonDeal) {
//	String retImage = null;
//	String affLink = getStringFromJsonAtt(jsonDeal,"trackingLink");
//	log.debug("selectImage affLink="+ affLink);
//	String finalAffUrl = null;
//	try {
//		finalAffUrl = ImageExtractor.calcFinalRedirectedUrl(affLink);
//		retImage = ImageExtractor.extractImageUrl(finalAffUrl);
//	} catch (IOException e) {
//		log.error("Failed to find final url for:"+ affLink,e);
//	}
//	log.debug("selectImage retImage="+ retImage);
//	return retImage;
//}

//	private String formatDate(String stringFromJsonAtt) {
//		String retCleanDate = stringFromJsonAtt.replaceAll(arg0, arg1)
//		retCleanDate = retCleanDate.replaceAll("Z","");//remove the Z
//		return retCleanDate;
//	}

//	private String selectImage(JsonObject jsonDeal) {
//		String retImage = null;
//		String affLink = getStringFromJsonAtt(jsonDeal,"trackingLink");
//		//Get the
//		log.debug("selectImage retImage="+ retImage);
//		return retImage;
//	}

	@Override
	protected JsonObject getJsonObj(String fetchFromUrl) throws MalformedURLException, IOException, JSONException {
		log.debug("fetchFromUrl="+fetchFromUrl);
		URL url = new URL(fetchFromUrl);
			
		URLConnection connection = url.openConnection();
		connection.setConnectTimeout(5000);
		connection.setReadTimeout(5000);
	
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(),Charset.forName("UTF-8")));
      
        String inputLine;
        StringBuilder sb = new StringBuilder();
        while ((inputLine = in.readLine()) != null){
        	sb.append(inputLine);
        }
        in.close();
	 	
        String jsonString = sb.toString();
       
        JsonParser jsonParser = new JsonParser();
		JsonObject xmlJSONObj = (JsonObject)jsonParser .parse(jsonString);
        
	 return xmlJSONObj;
	}

	@Override
	protected JsonArray getItemsJsonArray(JsonObject responseJson) {
		JsonArray retArr = null;
		if(responseJson.getAsJsonArray("incentives") == null){
			log.debug("NO Items returned from deals in response:" +responseJson );
			return retArr; 
		}

		retArr = responseJson.getAsJsonArray("incentives");
		return retArr;
	}

	


}
