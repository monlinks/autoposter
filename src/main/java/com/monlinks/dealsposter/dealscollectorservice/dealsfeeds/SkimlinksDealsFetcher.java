package com.monlinks.dealsposter.dealscollectorservice.dealsfeeds;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.Merchant;
import com.monlinks.dealsposter.model.MerchantLocation;
import com.monlinks.dealsposter.model.common.AudienceLocationEnum;
import com.monlinks.dealsposter.model.publisher.PublishAction;
import com.monlinks.dealsposter.utils.DateFormatConvertor;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

public class SkimlinksDealsFetcher extends AbsDealsFetcher {
	private static final int MAX_DEALS_FORM_MERCHANT_FOR_CURRENT_COLLECT = 5;
	private static Logger log = LogManager.getLogger(SkimlinksDealsFetcher.class.getName());
	WebDriver driver;
	String username = "buy2usa1@gmail.com";
	String password = "lior1lior";
	String loginUrl = "https://hub.skimlinks.com/login";

	String getDealsUrl = "https://hub.skimlinks.com/offers?merchantType=favourite&search=&sortBy=offer_ends&sortDir=asc&vertical=%s";//168==FASHION
	private Map<String, String> cookies;
	private String getDealsByMerchantNameUrl = "https://hub.skimlinks.com/offers?merchantType=favourite&search=%s&sortBy=offer_ends&sortDir=asc";

	public SkimlinksDealsFetcher() {
		super();
		try {

			buildDriver();

		} catch (Exception e) {
			log.error("Failed in init PhantomJSDriver", e);
		}
	}


	private void buildDriver() {
		String phantomJSDriverLocation = ResourceBundle.getBundle("application", Locale.ROOT).getString("phantomJSDriverLocation");
		String phantomJsPort = ResourceBundle.getBundle("application", Locale.ROOT).getString("phantomJsPort");
		File pahntomJsFile = new File(phantomJSDriverLocation);
		if (!pahntomJsFile.exists()) {
			//if not found (linux config) use windows location
			pahntomJsFile = new File("C:/Users/lior/DEV/autoPoster_DIY3/phantomjs/phantomjs.exe");
		}
		//System.setProperty("phantomjs.binary.path", phantomJSDriverLocation);

		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setJavascriptEnabled(true);
		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, phantomJSDriverLocation);

		driver = new PhantomJSDriver((new PhantomJSDriverService.Builder())
				//.usingPhantomJSExecutable(PhantomJSDriverService.findPhantomJS(caps, "https://github.com/ariya/phantomjs/wiki", "http://phantomjs.org/download.html")),	"https://github.com/detro/ghostdriver/downloads"))
				.usingPhantomJSExecutable(pahntomJsFile)
				.usingPort(Integer.parseInt(phantomJsPort))
				.withProxy(null)
						//.withLogFile(new File("phantomjsdriver.log"))
						//.usingCommandLineArguments(PhantomJSDriverService.findCLIArgumentsFromCaps(caps, "phantomjs.cli.args"))
						//.usingGhostDriverCommandLineArguments(PhantomJSDriverService.findCLIArgumentsFromCaps(caps, "phantomjs.ghostdriver.cli.args")
				.build(), caps);

	}

	private void performLogin() {

		driver.navigate().to("https://hub.skimlinks.com/login");

		WebElement element1 = driver.findElement(By.name("username"));
		element1.sendKeys("buy2usa1@gmail.com");

		WebElement element2 = driver.findElement(By.name("password"));
		element2.sendKeys("lior1lior");

		WebElement element3 = driver.findElement(By.id("btn-login"));
		element3.click();
	}


	@Override
	public List<Deal> fetchDealsByCategory(String category, MerchantLocation location) {
		performLogin();

		List<Deal> retDeals = new ArrayList<Deal>();
		String fetchUrl = String.format(getDealsUrl, category);
		log.debug("fetchUrl=" + fetchUrl);
		driver.navigate().to(fetchUrl);

		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("merchant-offers__card")));

		List<WebElement> merchantNameElements = driver.findElements(By.className("merchant-name"));
		List<WebElement> dealInfoElements = driver.findElements(By.className("offer-details"));

		String[] split = null;
		for (int i = 0; i < merchantNameElements.size(); i++) {
			WebElement nameElement = merchantNameElements.get(i);
			WebElement dealElement = dealInfoElements.get(i);

			split = dealElement.getText().split("\n");
			//System.out.println("split: \n" + split);
			Deal jsonDeal = buildDealJson(nameElement.getText(), split);
			retDeals.add(jsonDeal);
		}

		return retDeals;
	}

	private Deal buildDealJson(String merchantName, String[] dealElementAsString) {
		log.debug("dealElementAsString:" + Arrays.toString(dealElementAsString));
		Deal retDeal = null;
		String name = merchantName;
		String description = null;
		String dealDeepLink = null;
		String couponCode = null;
		String origEndDate = null;
		String formatedEndDate = null;
		String skimlinkId = "dummyId";

		try {
			String dealType = dealElementAsString[0];
			//take raw data from the page
			if ("COUPON".equals(dealType)) {
				description = dealElementAsString[1];
				dealDeepLink = dealElementAsString[2];
				couponCode = dealElementAsString[3];
				origEndDate = dealElementAsString[5];
			} else {//SALES , FREE SHIPPING
				description = dealElementAsString[1];
				dealDeepLink = dealElementAsString[2];
				origEndDate = dealElementAsString[4];
			}

			//Manipulate the data to fit the deal object
			//fix the date
			if (origEndDate != null) {
				String cleanEndDate = origEndDate.split("End: ")[1];
				log.debug("cleanEndDate=" + cleanEndDate);

				//we need to change the format from this:01 Jul 2017 to this: 2017-04-13 23:59:59
				SimpleDateFormat dateInputFormat = new SimpleDateFormat("dd MMM yyyy");
				SimpleDateFormat dateOutputFormat = new SimpleDateFormat("yyyy-MM-dd");//should be  "2017-04-13 23:59:59",
				DateFormatConvertor dateFormatConvertor = new DateFormatConvertor(dateInputFormat, dateOutputFormat);
				formatedEndDate = dateFormatConvertor.convert(cleanEndDate) + " 23:59:59";

				log.debug("formatedEndDate=" + formatedEndDate);
			}
			//fix the code
			if (couponCode != null) {
				couponCode = couponCode.replace("Code: ", "");
			}

			//fix the deepLink (remove the skimlink id

			if (dealDeepLink != null) {
				String[] split = dealDeepLink.split("skimoffer=");
				dealDeepLink = split[0];
				skimlinkId = split[1];
			}

			//take skimlinkId as extId
			String printFormat = "name=%s, desc=%s, link=%s, code=%s, endDate=%s";

			log.debug("****** Found Deal:" + String.format(printFormat, name, description, dealDeepLink, couponCode, origEndDate));

			String collectDate = LocalDateTime.now().toString();

			ArrayList<PublishAction> postedBy = new ArrayList<PublishAction>();

			ArrayList<AudienceLocationEnum> audianceLocations = new ArrayList<AudienceLocationEnum>();
			audianceLocations.add(AudienceLocationEnum.IL);
			audianceLocations.add(AudienceLocationEnum.US);

			retDeal = new Deal(skimlinkId, new Merchant(name, dealDeepLink, MerchantLocation.US, null), description, couponCode, null, null, null, null, formatedEndDate, description, null, collectDate, "SKIMLINKS", "", dealDeepLink, postedBy, audianceLocations);
			log.debug(retDeal);
		} catch (Exception e) {
			log.error(e);
		}
		return retDeal;
	}


	@Override
	public List<Deal> fetchDealsByList(String merchantsList, MerchantLocation merchantLocation) {
		performLogin();

		List<Deal> retDeals = new ArrayList<Deal>();
		final String[] merchantsNames = merchantsList.split(",");

		List<Deal> merchantDeals = new ArrayList<Deal>();
		for (String merchantName : merchantsNames) {
			try {
				log.debug("*************** Going to collect deals for " + merchantName);
				merchantDeals = fetchDealsForMerchant(merchantName);
				retDeals.addAll(merchantDeals);
				try {
					log.debug("************** Wait for 5 seconds");
					Thread.sleep(5000);//sleep 10 sec between calls
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}catch (Exception e){
				log.error("Problem fetching deals for :"+merchantName,e );
			}
		}

		return retDeals;
	}

	private List<Deal> fetchDealsForMerchant(String merchantName) throws UnsupportedEncodingException {
		List<Deal> retDeals = new ArrayList<Deal>();
		String fetchUrl = String.format(getDealsByMerchantNameUrl, URLEncoder.encode(merchantName,"UTF-8"));
		log.debug("fetchUrl=" + fetchUrl);
		driver.navigate().to(fetchUrl);

		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("merchant-offers__card")));

		if(element != null) {

			List<WebElement> merchantNameElements = driver.findElements(By.className("merchant-name"));
			List<WebElement> dealInfoElements = driver.findElements(By.className("offer-details"));

			String[] split = null;
			int numberOfDealsToCollect = Math.min(merchantNameElements.size(),MAX_DEALS_FORM_MERCHANT_FOR_CURRENT_COLLECT);
			log.debug("Number of deals found:"+merchantNameElements.size() );
			log.debug("number Of Deals To Collect :"+numberOfDealsToCollect );

			for (int i = 0; i < numberOfDealsToCollect; i++) {
				WebElement nameElement = merchantNameElements.get(i);
				WebElement dealElement = dealInfoElements.get(i);

				split = dealElement.getText().split("\n");
				//System.out.println("split: \n" + split);
				Deal jsonDeal = buildDealJson(nameElement.getText(), split);
				retDeals.add(jsonDeal);
			}

		}else{
			log.warn("No deals in HTML for merchant:"+ merchantName);
		}


		return retDeals;
	}


	@Override
	protected Deal createDeal(JsonObject categoryDeal, MerchantLocation location) {
		return null;
	}

	@Override
	protected JsonObject getJsonObj(String url) throws MalformedURLException, IOException, JSONException {
		return null;
	}

	@Override
	protected JsonArray getItemsJsonArray(JsonObject responseJson) {
		return null;
	}
}
