package com.monlinks.dealsposter.dealscollectorservice.dealsfeeds;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.amazon.AmazonRequestBuilder;
import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.Merchant;
import com.monlinks.dealsposter.model.MerchantLocation;
import com.monlinks.dealsposter.model.common.AudienceLocationEnum;
import com.monlinks.dealsposter.model.publisher.PublishAction;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by liorsh on 11/9/2017.
 */

public class AmazonDealsFetcher extends AbsDealsFetcher {
    private static Logger log = LogManager.getLogger(AmazonDealsFetcher.class.getName());

    AmazonRequestBuilder amazonRequestBuilder;

    public AmazonDealsFetcher() {
        try {
            amazonRequestBuilder = new AmazonRequestBuilder();
        } catch (Exception e) {
            log.error("Failed to build AmazonDealsFetcher.",e);
        }
    }

    @Override
    protected Deal createDeal(JsonObject jsonDeal, MerchantLocation location2) {
        String extId = getStringFromJsonAtt(jsonDeal,"ASIN");
        String merchantName = "Amazon";
        String merchantdomain = "http://amazon.com";
        MerchantLocation location = location2;
        String imageUrl = null;
        Merchant merchant = new Merchant(merchantName, merchantdomain, location, imageUrl);

        String description = getStringFromJsonAtt(jsonDeal,"Title");
        String code = null;//No codes in groupon
        String discount = null;
        String dealImageUrl = null;
        String postedDate = null;// not posted yet
        String startDate = null;// endAt: "2017-03-30T20:59:59Z", startAt: "2017-01-11T22:00:00Z",
        String endDate = null;
        String title = getStringFromJsonAtt(jsonDeal,"Title");

        String affUrl = getStringFromJsonAtt(jsonDeal,"DetailPageURL");
        String collectDate = LocalDateTime.now().toString();
        String dealSource= "amazon.com";

        String deepLink = affUrl;
        ArrayList<PublishAction> postedBy = new ArrayList<PublishAction>();

        ArrayList<AudienceLocationEnum> audianceLocations = new ArrayList<AudienceLocationEnum>();
        //US only
        audianceLocations.add(AudienceLocationEnum.US);

        Deal dealToInsert = new Deal(extId, merchant, description, code, discount, dealImageUrl, postedDate, startDate,	endDate, title,affUrl, collectDate, dealSource  , null,deepLink, postedBy,audianceLocations);
        return dealToInsert;
    }

    @Override
    protected JsonObject getJsonObj(String urlStr) throws MalformedURLException, IOException, JSONException {
        String respStr = getResponseFromUrl(urlStr);
        JSONObject jsonObject = XML.toJSONObject(respStr);

        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject)jsonParser.parse(jsonObject.toString());

        return gsonObject;
    }

    private String getResponseFromUrl(String urlStr) throws IOException {
        URL url = new URL(urlStr);

        URLConnection connection = url.openConnection();
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));

        String inputLine;
        StringBuilder sb = new StringBuilder();
        while ((inputLine = in.readLine()) != null){
            sb.append(inputLine);
        }
        in.close();

        String respString = sb.toString();
        return respString;
    }

    @Override
    protected JsonArray getItemsJsonArray(JsonObject responseJson) {
        JsonArray items = responseJson.getAsJsonArray("Items");
        return items;
    }

    @Override
    public List<Deal> fetchDealsByCategory(String category, MerchantLocation location) {

        String reqUrl = amazonRequestBuilder.buildSearchReqUrl(category, "\"black Friday\"");
        log.debug("reqUrl="+reqUrl);
        JsonArray amazonDeals = fetchDealsArrayFromUrl(reqUrl);
        List<Deal> retDeals = convertJsonToDeals(amazonDeals,location);
        return retDeals;
    }

    @Override
    public List<Deal> fetchDealsByList(String merchantsList, MerchantLocation merchantLocation) {
        return null;
    }
}
