package com.monlinks.dealsposter.dealscollectorservice.dealsfeeds;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.Merchant;
import com.monlinks.dealsposter.model.MerchantLocation;
import com.monlinks.dealsposter.model.common.AudienceLocationEnum;
import com.monlinks.dealsposter.model.publisher.PublishAction;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONException;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

public class GrouponILDealsFetcher extends AbsDealsFetcher {
	private static Logger log = LogManager.getLogger(GrouponILDealsFetcher.class.getName());
	//Affiliate Id	202128 ,Media Id 515
	String username = "tova"; 
	String password = "12345678";
	String loginUrl = "https://affiliate.grouponisrael.co.il/affiliate/server/1.0/loginUser?userName=tova&password=12345678";//POST parameters: userName=tova , password=12345678
	//Category: 869 תירות
	String getDealsUrl = "https://affiliate.grouponisrael.co.il/affiliate/server/1.0/getProducts?token=%s&categoryId=%s";//POST parameters: token=<from login response> , categoryId=
	String currentToken = "";
	
//	static HashMap<CategoriesEnums, String> categoryMap = new HashMap<CategoriesEnums, String>();
//	static{
//		categoryMap.put(CategoriesEnums.TRAVEL, "869");
//	}
	
		
	public GrouponILDealsFetcher() {
		super();

	}

	
	/**
	 * {
"status": 1,
"err": "",
"data": {
"id": "1",
"user_name": "test",
"campaign_source": "aaa",
"campaign_medium": "bb",
"campaign_name": "ccc",
"lock_to_ip": "1",
"ip_address": "192.168.2.1, 192.168.2.2, 192.168.2.3",
"receive_data_by": "1",
"last_login": "1483461437",
"last_update": "1483461157",
"token": "e023871c0447cc190c0c00a99a7d0d09"
},
"message": ""
}
	 * @return
	 */
	private String getToken() {
		String retToken = "";
		try {
			JsonObject responseObj = getJsonObj(loginUrl);
			if(responseObj.get("status").getAsInt() == 1){
				retToken = responseObj.getAsJsonObject("data").get("token").getAsString();
			}else{
				log.error("Groupon IL login returned error:"+ responseObj);
			}
		} catch (IOException | JSONException e) {
			log.error("Groupon IL login failed: "+ loginUrl,e);
		}
		
		return retToken;
	}

	@Override
	public List<Deal> fetchDealsByCategory(String category, MerchantLocation location) {
		currentToken = getToken();

		String fetchUrl = String.format(getDealsUrl, currentToken,category);
		log.debug("fetchUrl="+fetchUrl);
		JsonArray grouponsDeals = fetchDealsArrayFromUrl(fetchUrl);
		List<Deal> retDeals = convertJsonToDeals(grouponsDeals,location);
		return retDeals;
	}
	
	@Override
	protected JsonObject getJsonObj(String fetchFromUrl) throws MalformedURLException, IOException, JSONException {
	 	
        String jsonString = fetchPost(fetchFromUrl);
       
        JsonParser jsonParser = new JsonParser();
		JsonObject xmlJSONObj = (JsonObject)jsonParser .parse(jsonString);
        
	 return xmlJSONObj;
	}
	
	private String fetchPost(String fetchFromUrl) throws IOException{
		log.debug("fetchFromUrl="+fetchFromUrl);
	
		
		String resorceUrl = fetchFromUrl.split("\\?")[0];
		log.debug("resorceUrl="+resorceUrl);
		String parameters = fetchFromUrl.split("\\?")[1];
		log.debug("parameters="+parameters);
		
		URL url = new URL(resorceUrl);
		
		HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
		//connection.setConnectTimeout(1000);
		//connection.setReadTimeout(1000);
		
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);//make request POST
		connection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
		connection.setRequestProperty("Content-Length", String.valueOf(parameters.getBytes("UTF-8").length));
	        
		connection.getOutputStream().write(parameters.getBytes("UTF-8"));
	
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(),Charset.forName("UTF-8")));
      
        String inputLine;
        StringBuilder sb = new StringBuilder();
        while ((inputLine = in.readLine()) != null){
        	sb.append(inputLine);
        }
        in.close();
	 	
        String responseStr = sb.toString();
        
        return responseStr;
		
	}

	@Override
	protected JsonArray getItemsJsonArray(JsonObject responseJson) {
		JsonArray retArr = null;
		try {
			if (responseJson.getAsJsonObject("data") == null) {
				log.debug("NO Items returned from deals in response:" + responseJson);
				return retArr;
			}

			JsonObject dataObj = responseJson.getAsJsonObject("data");
			retArr = convertObjectToArray(dataObj);
		}catch (Exception e){
			log.error("Problem getting ItemsJsonArray. Orig responseJson: "+ responseJson.toString(),e);
		}
		return retArr;
	}

	private JsonArray convertObjectToArray(JsonObject dataObj) {
		JsonArray retArray = new JsonArray();
		
		for (Entry<String, JsonElement> jsonElement : dataObj.entrySet()) {
			retArray.add(jsonElement.getValue());
		}
		return retArray;
	}


	@Override
	public List<Deal> fetchDealsByList(String merchantsList, MerchantLocation merchantLocation) {
		log.error("NOT IMPLEMENTED");
		return null;
	}
	
	/**
	 *       "1002645":{  
         "id":"1002645",
         "title":"חבילת נופש הכוללת טיסות ו-7 לילות במלון לבחירה בפונטה קאנה, ע"         ב הכול כלול,
         החל מ-$1,
         229         לאדם!",
         "short_title":"חופשה ברפובליקה הדומיניקנית",
         "newsletter_title":"חופשה ברפובליקה הדומיניקנית, כולל שבועות",
         "sub_title":"בסט טורס",
         "short_description":"<p>חופשה אקזוטית ברפובליקה הדומיניקנית, כולל טיסות סדירות ו-7 לילות במלון 4 או 5 כוכבים לבחירה בפונטה קאנה, ע&rdquo;ב הכול כלול.&nbsp;<strong>כולל שבועות!</strong></p> 
",
         "purchase_txt":"",
         "area_idArr":[  

         ],
         "prefix_price":"",
         "suffix_price":"לאדם",
         "price":"4694",
         "old_price":"4694",
         "image":"/_media/media/2536/100056.jpg?t=1490025552",
         "category_id":"892",
         "category_treeArr":[  
            {  
               "id":869,
               "title":"חבילות נופש בחו"               ל",
               "order_num":1
            },
            {  
               "id":892,
               "title":"חופשות ביעדים אקזוטיים ",
               "order_num":2
            }
         ],
         "full_width":0,
         "top_category_id":"869",
         "deep_link":"igroupon://?page=groupon&page_id=1002645",
         "location_text":"",
         "gps":[  

         ],
         "badgesArr":[  

         ],
         "order_num":3,
         "to_ts":"1496696400",
         "date_live":"1489528800",
         "product_url":"https://www.grouponisrael.co.il/deal/1002645/dominican-republic-vacation/?affiliate_id=5&utm_term=20_892_1002645"
      },
	 */
	protected Deal createDeal(JsonObject jsonDeal, MerchantLocation location2)  {
		log.debug("Create Deal from jsonDeal="+ jsonDeal);
		String extId = getStringFromJsonAtt(jsonDeal,"id"); 
		String merchantName = "גרופון";
		String merchantdomain = "http://grouponisrael.co.il"; 
		MerchantLocation location = MerchantLocation.IL;
		String imageUrl = null;
		Merchant merchant = new Merchant(merchantName, merchantdomain, location, imageUrl);

		String description = getStringFromJsonAtt(jsonDeal,"newsletter_title");
		String code = null;//No codes in groupon
		String discount = null;
		String dealImageUrl = selectImage(jsonDeal); 
		String postedDate = null;// not posted yet
		String startDate = makeDate(getStringFromJsonAtt(jsonDeal,"date_live"));// "date_live":"1489528800",
		String endDate = makeDate(getStringFromJsonAtt(jsonDeal,"to_ts")); // "to_ts":"1496696400",
		String title = getStringFromJsonAtt(jsonDeal,"newsletter_title"); 
		
		//https://www.grouponisrael.co.il/?iTrack=9SJGceTM6SvJ16j_3MPsDGAXEhcKZCt_Ts9SJGceTM6SvJ16jtS&param=monlinks
		String affUrl = getStringFromJsonAtt(jsonDeal,"product_url")+"&iTrack=9SJGceTM6SvJ16j_3MPsDGAXEhcKZCt_Ts9SJGceTM6SvJ16jtS&param=monlinks";
		String collectDate =LocalDateTime.now().toString();
		String dealSource= "grouponisrael.co.il";
				
		String deepLink = affUrl;
		ArrayList<PublishAction> postedBy = new ArrayList<PublishAction>();
		ArrayList<AudienceLocationEnum> audianceLocations = new ArrayList<AudienceLocationEnum>();
		//IL only
		audianceLocations.add(AudienceLocationEnum.IL);
				
		Deal dealToInsert = new Deal(extId, merchant, description, code, discount, dealImageUrl, postedDate, startDate,	endDate, title,affUrl, collectDate, dealSource  , null,deepLink, postedBy,audianceLocations);
		return dealToInsert;
	}
	
	private String makeDate(String stringFromJsonAtt) {
		log.debug("stringFromJsonAtt="+stringFromJsonAtt);
		String retCleanDate = stringFromJsonAtt;
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date date = new Date(Long.parseLong(stringFromJsonAtt)* 1000L);
		retCleanDate = dateFormat.format(date);
				
		log.debug("retCleanDate="+retCleanDate);
		return retCleanDate;
	}

	private String selectImage(JsonObject jsonDeal) {
		String retImage = null;
		retImage = "https://grouponisrael.co.il"+getStringFromJsonAtt(jsonDeal,"image");
		if(retImage != null){
			log.debug("selectImage retImage="+ retImage +" for deal="+ jsonDeal);
		}
		return retImage;
	}

	


}
