package com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.amazon;

/**
 * Created by liorsh on 11/9/2017.
 */


        import java.io.UnsupportedEncodingException;
        import java.security.InvalidKeyException;
        import java.security.NoSuchAlgorithmException;
        import java.util.HashMap;
        import java.util.Map;

        import javax.xml.parsers.DocumentBuilder;
        import javax.xml.parsers.DocumentBuilderFactory;

        import org.w3c.dom.Document;
        import org.w3c.dom.Node;

/*
 * This class shows how to make a simple authenticated call to the
 * Amazon Product Advertising API.
 *
 * See the README.html that came with this sample for instructions on
 * configuring and running the sample.
 */
public class AmazonRequestBuilder {

    /*
     * Your Access Key ID, as taken from the Your Account page.
     */
    private static final String ACCESS_KEY_ID = "AKIAJWY6VYQLFSSLROYA";

    /*
     * Your Secret Key corresponding to the above ID, as taken from the
     * Your Account page.
     */
    private static final String SECRET_KEY = "tZduzSNjc2GGK94qEl6ikIHElgc5rTO4CWXuUmFC";

    /*
     * Use the end-point according to the region you are interested in.
     */
    private static final String ENDPOINT = "webservices.amazon.com";
    private SignedRequestsHelper helper;

    public AmazonRequestBuilder() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
            helper = SignedRequestsHelper.getInstance(ENDPOINT, ACCESS_KEY_ID, SECRET_KEY);
    }

    public String buildSearchReqUrl(String searchIndex,String keywords){//SearchIndex:Electronics
        String requestUrl = null;

        Map<String, String> params = new HashMap<String, String>();

        params.put("Service", "AWSECommerceService");
        params.put("Operation", "ItemSearch");
        params.put("AWSAccessKeyId", "AKIAJWY6VYQLFSSLROYA");
        params.put("AssociateTag", "123poster-20");
        params.put("SearchIndex", searchIndex);
        params.put("Keywords", keywords);
        //params.put("ResponseGroup", "Images,ItemAttributes,Offers");

        requestUrl = helper.sign(params);

        return requestUrl;
    }

//    public static void main(String[] args) {
//
//        /*
//         * Set up the signed requests helper.
//         */
//        SignedRequestsHelper helper;
//
//        try {
//            helper = SignedRequestsHelper.getInstance(ENDPOINT, ACCESS_KEY_ID, SECRET_KEY);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return;
//        }
//
//        String requestUrl = null;
//
//        Map<String, String> params = new HashMap<String, String>();
//
//        params.put("Service", "AWSECommerceService");
//        params.put("Operation", "ItemSearch");
//        params.put("AWSAccessKeyId", "AKIAJWY6VYQLFSSLROYA");
//        params.put("AssociateTag", "123poster-20");
//        params.put("SearchIndex", "Electronics");
//        params.put("Keywords", "black Friday deals 2017");
//        //params.put("ResponseGroup", "Images,ItemAttributes,Offers");
//
//        requestUrl = helper.sign(params);
//
//        System.out.println("Signed URL: \"" + requestUrl + "\"");
//    }
}