package com.monlinks.dealsposter.dealscollectorservice.dealsfeeds;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.Merchant;
import com.monlinks.dealsposter.model.MerchantLocation;
import com.monlinks.dealsposter.model.common.AudienceLocationEnum;
import com.monlinks.dealsposter.model.publisher.PublishAction;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class GrouponUSDealsFetcher extends AbsDealsFetcher {
	private static Logger log = LogManager.getLogger(GrouponUSDealsFetcher.class.getName());
	//Affiliate Id	202128 ,Media Id 515
	//String dealFetchUrlTemplate = "https://partner-int-api.groupon.com/deals.json?country_code=IL&tsToken=IL_AFF_0_202128_515_0&filters=topcategory:%s&offset=0&limit=30&sid=monlinks";
	//groupon US
	//shop2supportIsrael: Affiliate Id 204606
	String dealFetchUrlTemplate = "https://partner-api.groupon.com/deals.json?tsToken=US_AFF_0_204606_212556_0&filters=category:%s&offset=0&limit=30&sid=monlinks";
	
	@Override
	public List<Deal> fetchDealsByCategory(String category, MerchantLocation location) {
		String formatedUrl = String.format(dealFetchUrlTemplate, category);
		JsonArray grouponsDeals = fetchDealsArrayFromUrl(formatedUrl);
		List<Deal> retDeals = convertJsonToDeals(grouponsDeals,location);
		return retDeals;
	}
	
	protected Deal createDeal(JsonObject jsonDeal, MerchantLocation location2)  {
		String extId = getStringFromJsonAtt(jsonDeal,"uuid"); 
		String merchantName = "Groupon";
		String merchantdomain = "http://groupon.com"; 
		MerchantLocation location = location2;
		String imageUrl = null;
		Merchant merchant = new Merchant(merchantName, merchantdomain, location, imageUrl);

		String description = getStringFromJsonAtt(jsonDeal,"title"); 
		String code = null;//No codes in groupon
		String discount = null;
		String dealImageUrl = selectImage(jsonDeal); 
		String postedDate = null;// not posted yet
		String startDate = cleanDate(getStringFromJsonAtt(jsonDeal,"startAt"));// endAt: "2017-03-30T20:59:59Z", startAt: "2017-01-11T22:00:00Z",
		String endDate = cleanDate(getStringFromJsonAtt(jsonDeal,"endAt")); 
		String title = getStringFromJsonAtt(jsonDeal,"shortAnnouncementTitle"); 
		
		String affUrl = getStringFromJsonAtt(jsonDeal,"dealUrl");
		String collectDate =LocalDateTime.now().toString();
		String dealSource= "groupon.com";
				
		String deepLink = affUrl;
		ArrayList<PublishAction> postedBy = new ArrayList<PublishAction>();
		
		ArrayList<AudienceLocationEnum> audianceLocations = new ArrayList<AudienceLocationEnum>();
		//US only
		audianceLocations.add(AudienceLocationEnum.US);
		
		Deal dealToInsert = new Deal(extId, merchant, description, code, discount, dealImageUrl, postedDate, startDate,	endDate, title,affUrl, collectDate, dealSource  , null,deepLink, postedBy,audianceLocations);
		return dealToInsert;
	}
	
	private String cleanDate(String stringFromJsonAtt) {
		String retCleanDate = stringFromJsonAtt.replaceAll("T"," ");//remove the T
		retCleanDate = retCleanDate.replaceAll("Z","");//remove the Z
		return retCleanDate;
	}

	private String selectImage(JsonObject jsonDeal) {
		String retImage = null;
		retImage = getStringFromJsonAtt(jsonDeal,"largeImageUrl");
		if(retImage != null){
			log.debug("selectImage retImage="+ retImage +" for deal="+ jsonDeal);
		}
		return retImage;
	}

	@Override
	protected JsonObject getJsonObj(String fetchFromUrl) throws MalformedURLException, IOException, JSONException {
		log.debug("fetchFromUrl="+fetchFromUrl);
		URL url = new URL(fetchFromUrl);
			
		URLConnection connection = url.openConnection();
		connection.setConnectTimeout(5000);
		connection.setReadTimeout(5000);
	
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(),Charset.forName("UTF-8")));
      
        String inputLine;
        StringBuilder sb = new StringBuilder();
        while ((inputLine = in.readLine()) != null){
        	sb.append(inputLine);
        }
        in.close();
	 	
        String jsonString = sb.toString();
       
        JsonParser jsonParser = new JsonParser();
		JsonObject xmlJSONObj = (JsonObject)jsonParser .parse(jsonString);
        
	 return xmlJSONObj;
	}

	@Override
	protected JsonArray getItemsJsonArray(JsonObject responseJson) {
		JsonArray retArr = null;
		if(responseJson.getAsJsonArray("deals") == null){
			log.debug("NO Items returned from deals in response:" +responseJson );
			return retArr; 
		}

		retArr = responseJson.getAsJsonArray("deals");
		return retArr;
	}

	@Override
	public List<Deal> fetchDealsByList(String merchantsList, MerchantLocation merchantLocation) {
		log.error("NOT IMPLEMENTED");
		return null;
	}


}
