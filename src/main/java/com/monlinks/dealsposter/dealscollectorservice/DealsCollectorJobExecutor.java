package com.monlinks.dealsposter.dealscollectorservice;

import com.monlinks.dealsposter.activity.ActivityAgregator;
import com.monlinks.dealsposter.activity.CollectorActivity;
import com.monlinks.dealsposter.dao.CollectorsRepository;
import com.monlinks.dealsposter.dao.DealsRepository;
import com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.IDealsFetcher;
import com.monlinks.dealsposter.model.CategoriesEnums;
import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.MerchantLocation;
import com.monlinks.dealsposter.model.collector.CollectType;
import com.monlinks.dealsposter.model.collector.Collector;
import com.monlinks.dealsposter.utils.ThreadDelay;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.*;


public class DealsCollectorJobExecutor implements Job {


	private DealsRepository dealsRepository;
	private CollectType collectType;
	private MerchantLocation merchantLocation;

	private ResourceBundle config = ResourceBundle.getBundle("application",Locale.ROOT);
	private boolean isRunningInTestMode = Boolean.parseBoolean(config.getString("collectDeals.test.mode"));
	private List<Deal> currentCollectedAndSavedNewDeals = new ArrayList<Deal>();
	private CollectorsRepository collectorsRepository;

	private static Logger log = LogManager.getLogger(DealsCollectorJobExecutor.class.getName());


	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("############### START DealsCollectorJobExecutor ########################");
		List<Collector> collectors = (List<Collector>) context.getJobDetail().getJobDataMap().get("collectors");
		this.dealsRepository = (DealsRepository) context.getJobDetail().getJobDataMap().get("dealsRepository");
		this.collectorsRepository = (CollectorsRepository) context.getJobDetail().getJobDataMap().get("collectorsRepository");

		doCollect(collectors);
		
		log.info("############### FINISH DealsCollectorJobExecutor ########################");
	}

	public List<Deal> runCollectDirectly(List<Collector> collectors,DealsRepository dealsRepository,CollectorsRepository collectorsRepository){
		log.info("############### START DealsCollectorJobExecutor (runCollectDirectly) ########################");
		this.dealsRepository = dealsRepository;
		this.collectorsRepository = collectorsRepository;
		doCollect(collectors);

		log.info("############### FINISH DealsCollectorJobExecutor (runCollectDirectly) ########################");

		return currentCollectedAndSavedNewDeals;
	}

	private void doCollect(List<Collector> collectors) {
		currentCollectedAndSavedNewDeals = new ArrayList<Deal>();//clean the list

		if(isRunningInTestMode) log.info("RUNNING IN TEST MODE");

		log.debug("BEFORE collect: currentCollectedAndSavedNewDeals="+ currentCollectedAndSavedNewDeals);

		for (Collector collector : collectors) {
			handleCollect2(collector);
			ThreadDelay.delayInSeconds(5);
		}

		log.debug("AFTER collect: currentCollectedAndSavedNewDeals="+ currentCollectedAndSavedNewDeals);
	}


	public void handleCollect2(Collector collector) {
		log.info("START collector "+ collector.getName());
		try {
			
			List<Deal>  dealsList = getJsonDeals(collector);
			if(dealsList == null || dealsList.size()==0){
				log.info("FINISH collector "+ collector.getName() +": NO deals were found, will not update DB.Collector="+ collector);
				return;
			}
			
			List<Deal> newSavedDeals = saveDeals(dealsList,collector.getName(),collector.getCategoryToBeSavedWith());
			
			log.debug("FINISHED collect and save "+newSavedDeals.size()+" new deals:"+newSavedDeals);
			
			log.info("FINISH collector "+ collector.getName() +": Collected "+ dealsList.size() +" deals. Saved "+ newSavedDeals.size() +" new deals.");
			
			currentCollectedAndSavedNewDeals.addAll(newSavedDeals);

			ActivityAgregator.INSTANCE.addActivity(new CollectorActivity(collector.getName(),(new Date()).toString(),dealsList.size(),newSavedDeals.size()));
			
		} catch (Exception e) {
				log.error("Problem in collecting deals",e);
			}
	}



	private List<Deal> saveDeals(List<Deal> dealsList,String collectorName,CategoriesEnums categoryToBeSavedWith ) {
		List<Deal> newSavedDeals = new ArrayList<Deal>();
		//go over each deal, only if not exist add it to the DB
		for (Deal deal : dealsList) {
			Deal foundDeal = null;
			if(!isRunningInTestMode){
				foundDeal = dealsRepository.findOne(deal.getExtId());
			}
			
			if(foundDeal == null){
				deal.setCollectorName(collectorName);
				deal.setCategory(categoryToBeSavedWith);
							
				
				/*********** SAVE to DB ***********/
				if(isRunningInTestMode){
					log.info("Running in test mode: will not save new deal to DB");
					//log.info("Deal=" + deal);
				}else{
					log.debug("Save new deal to DB");
					dealsRepository.save(deal);
				}
				
				/**********************************/
				newSavedDeals.add(deal);
				log.debug("SAVED the deal:" + deal);
			}else{
				log.debug("Deal already exist:"+ deal.getExtId());
			}
		}
		
		return newSavedDeals;
		
	}

	private List<Deal>  getJsonDeals(Collector collector) {
		List<Deal> retArr = null;
		CollectType collectType = collector.getCollectType();
		IDealsFetcher dealsFetcher = collector.getDealsFetcher().getDealsFetcher();

		switch (collectType) {
		case CATEGORY:
			retArr = dealsFetcher.fetchDealsByCategory(collector.getCollectValue(), collector.getMerchantLocation());
			break;
		case MERCHANT_LIST:
			retArr = dealsFetcher.fetchDealsByList(collector.getCollectValue(), collector.getMerchantLocation());
			break;
		default:
			break;
		}
		
		int numberOfDeals = (retArr==null?0:retArr.size());
		log.debug("##### Found total "+ numberOfDeals +" deals.");
		
		return retArr;
	}

	
	public List<Deal> getCurrentCollectedAndSavedNewDeals() {
		return currentCollectedAndSavedNewDeals;
	}

	public void setCurrentCollectedAndSavedNewDeals(List<Deal> currentCollectedAndSavedNewDeals) {
		this.currentCollectedAndSavedNewDeals = currentCollectedAndSavedNewDeals;
	}

	public DealsRepository getDealsRepository() {
		return dealsRepository;
	}

	public void setDealsRepository(DealsRepository dealsRepository) {
		this.dealsRepository = dealsRepository;
	}


}
