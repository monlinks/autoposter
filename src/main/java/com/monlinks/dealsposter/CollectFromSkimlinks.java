package com.monlinks.dealsposter;

import com.monlinks.dealsposter.dao.DealsRepository;
import com.monlinks.dealsposter.dealscollectorservice.DealsCollectorJobExecutor;
import com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.DealFetchersEnum;
import com.monlinks.dealsposter.model.CategoriesEnums;
import com.monlinks.dealsposter.model.MerchantLocation;
import com.monlinks.dealsposter.model.collector.CollectType;
import com.monlinks.dealsposter.model.collector.Collector;
import com.monlinks.dealsposter.model.common.AudienceLocationEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;



@SpringBootApplication
// @EnableScheduling
public class CollectFromSkimlinks implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(CollectFromSkimlinks.class);

	String skim_fashion_list = "ebay,Shoes.com,NewBalance.com,Shoeaholics,bebe,Avenue,American Eagle Outfitters,Old Navy,The Children's Place,DisneyStore,Sierra Trading Post,Hanes.com,Gap,Nike Store,Joe's New Balance Outlet,Payless.com,Puma,Famous Footwear,Footlocker.com,Columbia Sportswear,Levis,Clarks USA,UGG,Timberland,Rockport,Ecco,Nautica.com,Urban Outfitters,Watch Shop,ShoeMetro,Road Runner Sports,Tbdress,Boohoo.com,Carter's,Macys,Target,Wal-Mart.com,Bloomingdale's";
	String skim_elect_list = "B&H Photo Video,Newegg.com,ThinkGeek,Canon,AliExpress,Lenovo,Dell,Homedepot,Best Buy,GeekBuying,Abt.com,Logitech,DealExtreme,Olympus,Sony Store,Panasonic,Dealsmachine.com,Gearbest,TinyDeal,DHGate,BuyinCoins,Everbuying.net";
	String skim_travel_list = "";


	@Autowired
	private DealsRepository repository;


	public static void main(String[] args) {
		System.out.println("START CollectFromSkimlinks");
		SpringApplication.run(CollectFromSkimlinks.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("START RUN CollectFromSkimlinks");

		DealsCollectorJobExecutor dealsCollectorJobExecutor = new DealsCollectorJobExecutor();

		dealsCollectorJobExecutor.setDealsRepository(repository);

		CategoriesEnums categoryToBeSavedWith = CategoriesEnums.FASHION;
		ArrayList<AudienceLocationEnum> targetAudienceLocationsAll = new ArrayList<AudienceLocationEnum>();
		targetAudienceLocationsAll.add(AudienceLocationEnum.IL);
		targetAudienceLocationsAll.add(AudienceLocationEnum.US);

		log.debug("############### START COLLECT FASHION");
		Collector fashionUSSkimlinksLISTCollector = new Collector("fashion_US_Skim_Collector_LIST", true, CollectType.MERCHANT_LIST, skim_fashion_list, MerchantLocation.US, targetAudienceLocationsAll, DealFetchersEnum.SKIMLINKS, categoryToBeSavedWith);
		dealsCollectorJobExecutor.handleCollect2(fashionUSSkimlinksLISTCollector);

		log.debug("############### START COLLECT ELECTRONICS");
		Collector electronicsUSSkimlinksLISTCollector = new Collector("electronic_US_Skim_Collector_LIST", true, CollectType.MERCHANT_LIST, skim_elect_list, MerchantLocation.US, targetAudienceLocationsAll, DealFetchersEnum.SKIMLINKS, CategoriesEnums.ELECTRONICS);
		dealsCollectorJobExecutor.handleCollect2(electronicsUSSkimlinksLISTCollector);

//		Collector fashionUSSkimlinksCollector = new Collector("fashion_US_Skim_Collector", true, CollectType.CATEGORY, "168", MerchantLocation.US, targetAudienceLocationsAll, DealFetchersEnum.SKIMLINKS, categoryToBeSavedWith);
//		dealsCollectorJobExecutor.handleCollect2(fashionUSSkimlinksCollector);
//
//		categoryToBeSavedWith = CategoriesEnums.TRAVEL;
//		Collector travelUSSkimlinksCollector = new Collector("travel_US_Skim_Collector", true, CollectType.CATEGORY, "182", MerchantLocation.US, targetAudienceLocationsAll, DealFetchersEnum.SKIMLINKS, categoryToBeSavedWith);
//
//		dealsCollectorJobExecutor.handleCollect2(travelUSSkimlinksCollector);
//
//		categoryToBeSavedWith = CategoriesEnums.ELECTRONICS;
//		Collector elecUSSkimlinksCollector = new Collector("electronics_US_Skim_Collector", true, CollectType.CATEGORY, "178", MerchantLocation.US, targetAudienceLocationsAll, DealFetchersEnum.SKIMLINKS, categoryToBeSavedWith);

//		dealsCollectorJobExecutor.handleCollect2(elecUSSkimlinksCollector);

	}

}