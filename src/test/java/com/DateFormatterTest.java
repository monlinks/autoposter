package com;

import com.monlinks.dealsposter.utils.DateFormatConvertor;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by lior on 09/05/2017.
 */
public class DateFormatterTest {
    private static Logger log = LogManager.getLogger(DateFormatterTest.class.getName());

    @Test
    @Ignore
    public void testConvert() throws ParseException {
        String cleanEndDate = "09 May 2017";//"10 May 2017";//origEndDate.split("End: ")[1];
        log.debug("cleanEndDate="+cleanEndDate);

        //we need to change the format from this:01 Jul 2017 to this: 2017-04-13 23:59:59
        SimpleDateFormat dateInputFormat = new SimpleDateFormat("dd MMM yyy");
        SimpleDateFormat dateOutputFormat = new SimpleDateFormat("yyyy-MM-dd");//should be  "2017-04-13 23:59:59",
        DateFormatConvertor dateFormatConvertor = new DateFormatConvertor(dateInputFormat,dateOutputFormat);
        String formatedEndDate = dateFormatConvertor.convert(cleanEndDate) + " 23:59:59";

        log.debug("formatedEndDate="+formatedEndDate);
    }
}
