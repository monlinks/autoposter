package com;

import com.monlinks.dealsposter.dealscollectorservice.DealsCollectorJobExecutor;
import com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.AmazonDealsFetcher;
import com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.DealFetchersEnum;
import com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.IDealsFetcher;
import com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.amazon.AmazonRequestBuilder;
import com.monlinks.dealsposter.model.CategoriesEnums;
import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.MerchantLocation;
import com.monlinks.dealsposter.model.collector.CollectType;
import com.monlinks.dealsposter.model.collector.Collector;
import com.monlinks.dealsposter.model.common.AudienceLocationEnum;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lior on 09/04/2017.
 */

public class AmazonFetcherTest {
    private static Logger log = LogManager.getLogger(AmazonFetcherTest.class.getName());


    @Test
    @Ignore
    public void testFetchDeals() {

//        try {
//            AmazonRequestBuilder amazonRequestBuilder = new AmazonRequestBuilder();
//            String electronicsUrl = amazonRequestBuilder.buildSearchReqUrl("Electronics", "black Friday");
//            log.debug("electronicsUrl="+electronicsUrl);
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (InvalidKeyException e) {
//            e.printStackTrace();
//        }

        IDealsFetcher df = new AmazonDealsFetcher();
        Assert.assertNotNull(df);

        List<Deal> electDeals = df.fetchDealsByCategory("Electronics", MerchantLocation.ALL);
        log.debug("ret deals="+electDeals);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<Deal> fashionDeals = df.fetchDealsByCategory("Electronics", MerchantLocation.ALL);
        log.debug("ret deals="+fashionDeals);


    }


    @Test
    @Ignore
    public void testRunCollectorDeals() {
        DealsCollectorJobExecutor dealsCollectorJobExecutor = new DealsCollectorJobExecutor();
//        DealsRepository dealsRepository = (DealsRepository) context.getBean("DealsRepository");
//        dealsCollectorJobExecutor.setDealsRepository(dealsRepository);

        CategoriesEnums categoryToBeSavedWith = CategoriesEnums.FASHION;
        ArrayList<AudienceLocationEnum> targetAudienceLocationsAll = new ArrayList<AudienceLocationEnum>();
        targetAudienceLocationsAll.add(AudienceLocationEnum.IL);
        targetAudienceLocationsAll.add(AudienceLocationEnum.US);

        Collector fashionUSSkimlinksCollector = new Collector("fashion_US_Skim_Collector", true, CollectType.CATEGORY, "168" , MerchantLocation.US, targetAudienceLocationsAll , DealFetchersEnum.SKIMLINKS, categoryToBeSavedWith );


        dealsCollectorJobExecutor.handleCollect2(fashionUSSkimlinksCollector);

    }

}
