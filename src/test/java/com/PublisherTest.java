package com;

import com.monlinks.dealsposter.dealsposterservice.DealsPublisherJobExecutor;
import com.monlinks.dealsposter.dealsposterservice.postsender.FacebookPostSender;
import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.Merchant;
import com.monlinks.dealsposter.model.publisher.ActionResultEnum;
import com.monlinks.dealsposter.model.publisher.PostingDestination;
import com.monlinks.dealsposter.model.publisher.PostingUser;
import com.monlinks.dealsposter.model.publisher.PublishAction;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.util.Assert;

import java.util.ArrayList;

/**
 * Created by lior on 16/05/2017.
 */
public class PublisherTest {
    private static Logger log = LogManager.getLogger(PublisherTest.class.getName());

    ArrayList<PublishAction> postedBy = new ArrayList<PublishAction>();

    @Test
    @Ignore
    public void testBuildFacebookPostToGroup() throws Exception {
        String userAuthKey = "EAACvVIo6h10BAMXu76PSTjuWePxgxHT9q2MvOkZBcfdT7ulOWEL5Sg19gEFeKAEZApkQVdUkMN2zDUXhtXzIjnPOJlPIjgaqBHLuFdMCvmulWjfgsj0CF7KYYzu8tcq16BgNvvtJ7ZBTn7dY0d6XHLFnAwzE9oZD";
        String publisherUserId = "monlinksPublisher";

        DealsPublisherJobExecutor dealsPublisherJobExecutor = new DealsPublisherJobExecutor();
        dealsPublisherJobExecutor.setRunningInTestMode(true);

//        String test = "test";
//        String affLink = "http://www.6pm.com/oakley/UgKKA-ICAQo.zso?s=recentSalesStyle/desc/&si=3647404&sy=1&";
//        log.debug("affLink="+affLink);

        String affLink = "https://www.grouponisrael.co.il/deal/1007287/august-ayia-napa/?affiliate_id=5&utm_term=20_877_1007287&iTrack=9SJGceTM6SvJ16j_3MPsDGAXEhcKZCt_Ts9SJGceTM6SvJ16jtS&param=monlinks";

        String deepLink = affLink;
       // affLink=null;

        String endDateStr = "2018-06-18 23:59:59";
        String shortTitle = "This is short title";
        String longTitle = "Best Western offers charming hotels ideally situated in York - each with a unique character of its own. Whether you want to be pampered in opulent luxury or just want a cosy home away from home, we have something to suit you. Historic York combined with Best Western’s attention to every detail will add up to a Great British city break you’ll remember forever.";

        Deal deal = new Deal("testDealId",new Merchant("test",deepLink,null,null),null,null,null,null,null,null,endDateStr,longTitle,affLink,null,null,null,deepLink,postedBy,null);
        FacebookPostSender facebookSender = new FacebookPostSender(new PostingUser("test","test",userAuthKey,"test"));
        facebookSender.setRunningInTestMode(true);

        PostingDestination postingDestination = new PostingDestination(PostingDestination.SocialNetworksEnum.FACEBOOK,
                PostingDestination.DestinationTypeEnum.GROUP, "testGroupId", "מבצעים לאתרי תירות", PostingDestination.PostLangEnum.HEB);


        PublishAction publishAction = dealsPublisherJobExecutor.publishTheDeal(deal, facebookSender, postingDestination, publisherUserId);

        log.debug("publishAction="+publishAction);
        Assert.notNull(publishAction);
        Assert.isTrue(publishAction.getActionResult().equals(ActionResultEnum.SUCCESS));

    }

    @Test
    @Ignore
    public void testBuildFacebookPostToPage() throws Exception {
        //page auth key :
        String userAuthKey = "EAACvVIo6h10BAKgStQmY2IycAJObdTxjVFIYLF6almbe45pFBXqPDuOU5g1ZAV3MOH6d3oP0s6YZCsR5AAaxdl7YAZBgDQofYeClN98iaGVCFABBOWTLrDDkZCjdM2KGFeLNmuikbJl0aLp64x5IGkHk37vw7hFQbh6EMc56EigPZB4rnZA8B8";
        //Alon: String userAuthKey = "EAACvVIo6h10BAPOpBwlm3ZCee8YwdZApvAVtBV2azmpjBdHUSZBMF13COgA7LsOOZAZBxwNq7oCsYsV2Tfh0tkyZAnPm343DXmjvZCEWCrMpZCjXvV2P9MHEF7Pq9jeYZBFbRnb2QxOKaNnkjXVZBrApfzBOBe5ix2N54ZD";

        String publisherUserId = "ushops";

        DealsPublisherJobExecutor dealsPublisherJobExecutor = new DealsPublisherJobExecutor();
        dealsPublisherJobExecutor.setRunningInTestMode(true);

//        String test = "test";
//        String affLink = "http://www.6pm.com/oakley/UgKKA-ICAQo.zso?s=recentSalesStyle/desc/&si=3647404&sy=1&";
//        log.debug("affLink="+affLink);

        String deepLink = "http://www.crocs.com/c/sale";
        // affLink=null;
        String affLink = null;

        String endDateStr = "2018-06-18 23:59:59";
        Deal deal = new Deal("testDealId",new Merchant("test",deepLink,null,null),"test description",null,null,null,null,null,endDateStr,"this is the title",affLink,null,null,null,deepLink,postedBy,null);
        FacebookPostSender facebookSender = new FacebookPostSender(new PostingUser("test","test",userAuthKey,"test"));
        //facebookSender.setRunningInTestMode(true);

        PostingDestination postingDestination = new PostingDestination(PostingDestination.SocialNetworksEnum.FACEBOOK,
                PostingDestination.DestinationTypeEnum.PAGE, "1973385039563970", "מבצעים ushops", PostingDestination.PostLangEnum.HEB);

        PublishAction publishAction = dealsPublisherJobExecutor.publishTheDeal(deal, facebookSender, postingDestination, publisherUserId);

        log.debug("publishAction="+publishAction);
        Assert.notNull(publishAction);
        Assert.isTrue(publishAction.getActionResult().equals(ActionResultEnum.SUCCESS));

    }
}
