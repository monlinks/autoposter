package com;

import com.monlinks.dealsposter.dealscollectorservice.DealsCollectorJobExecutor;
import com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.DealFetchersEnum;
import com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.IDealsFetcher;
import com.monlinks.dealsposter.dealscollectorservice.dealsfeeds.SkimlinksDealsFetcher;
import com.monlinks.dealsposter.model.CategoriesEnums;
import com.monlinks.dealsposter.model.Deal;
import com.monlinks.dealsposter.model.MerchantLocation;
import com.monlinks.dealsposter.model.collector.CollectType;
import com.monlinks.dealsposter.model.collector.Collector;
import com.monlinks.dealsposter.model.common.AudienceLocationEnum;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lior on 09/04/2017.
 */

public class SkimlinksFetcherTest {
    private static Logger log = LogManager.getLogger(SkimlinksFetcherTest.class.getName());


    @Test
    @Ignore
    public void testFetchDeals() {
        IDealsFetcher df = new SkimlinksDealsFetcher();
        Assert.assertNotNull(df);

        List<Deal> fashionDeals = df.fetchDealsByCategory("168", MerchantLocation.ALL);
        log.debug("ret deals="+fashionDeals);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<Deal> travelDeals = df.fetchDealsByCategory("182", MerchantLocation.ALL);
        log.debug("ret deals="+travelDeals);


    }


    @Test
    @Ignore
    public void testRunCollectorDeals() {
        DealsCollectorJobExecutor dealsCollectorJobExecutor = new DealsCollectorJobExecutor();
//        DealsRepository dealsRepository = (DealsRepository) context.getBean("DealsRepository");
//        dealsCollectorJobExecutor.setDealsRepository(dealsRepository);

        CategoriesEnums categoryToBeSavedWith = CategoriesEnums.FASHION;
        ArrayList<AudienceLocationEnum> targetAudienceLocationsAll = new ArrayList<AudienceLocationEnum>();
        targetAudienceLocationsAll.add(AudienceLocationEnum.IL);
        targetAudienceLocationsAll.add(AudienceLocationEnum.US);

        Collector fashionUSSkimlinksCollector = new Collector("fashion_US_Skim_Collector", true, CollectType.CATEGORY, "168" , MerchantLocation.US, targetAudienceLocationsAll , DealFetchersEnum.SKIMLINKS, categoryToBeSavedWith );


        dealsCollectorJobExecutor.handleCollect2(fashionUSSkimlinksCollector);

    }

}
