package com;

import com.monlinks.dealsposter.utils.AffiliationUrlNotValidException;
import com.monlinks.dealsposter.utils.UrlRelatedHelper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created by lior on 09/04/2017.
 */
public class UrlResolverTest {
    private static Logger log = LogManager.getLogger(UrlResolverTest.class.getName());

    @Test
    @Ignore
    public void testResolveAffUrl() {
        String affUrl = "http://prosperent.com/click/api/linkaffiliator/userId/416587/apikey/34e313e356738fa5d0f03cd70db9a8ea/location/http%3A%2F%2Ffacebook.com/url/http%3A%2F%2Fwww.walmart.com";
        String merchantDomain = "www.walmart.com";
       testOneResolveAffUrl(affUrl,merchantDomain);

        affUrl = "http://prosperent.com/click/api/linkaffiliator/userId/416587/apikey/34e313e356738fa5d0f03cd70db9a8ea/location/http%3A%2F%2Ffacebook.com/url/http%3A%2F%2Fuk.hotels.com%2Fhotel-rewards-pillar%2Fhotelscomrewards.html";
        //Final url in browser: https://uk.hotels.com/hotel-rewards-pillar/hotelscomrewards.html?tmid=hcom-us.dps.cj.package-.package&PSRC=AFF10&rffrid=aff.hcom.us.002.003.7330889.cjaff&wapa6=416587M1M170409103056WAF&ref_id=e8bab0921d4111e782b71803730cbbf1_878303730654568654%3Amm5xtRfWB5Ra&sub_publisher=2779388&sub_site=7330889&sub_ad=11886860
        merchantDomain = "uk.hotels.com";
        testOneResolveAffUrl(affUrl,merchantDomain);

        affUrl = "http://r.srvtrck.com/v1/redirect?type=incentive&id=7f46070e84f941f4b9f46018a3256b10&api_key=6817b3630fcc987430aebc097413a7b0&site_id=2e4c8f098e864b52a43eae165498314c&dch=feed&ad_t=coupon";
        merchantDomain = "nordstrom.com";
        testOneResolveAffUrl(affUrl,merchantDomain);

        affUrl = "http://prosperent.com/click/api/linkaffiliator/userId/416587/apikey/34e313e356738fa5d0f03cd70db9a8ea/location/http%3A%2F%2Ffacebook.com/url/http%3A%2F%2Fshop.lonelyplanet.com%2F";
        merchantDomain = "shop.lonelyplanet.com";
        testOneResolveAffUrl(affUrl,merchantDomain);


        affUrl = "http://r.srvtrck.com/v1/redirect?type=incentive&id=cfa812c799b548a0af3f4e2e64be598f&api_key=6817b3630fcc987430aebc097413a7b0&site_id=2e4c8f098e864b52a43eae165498314c&dch=feed&ad_t=coupon";
        merchantDomain = "nordstrom.com";
        testOneResolveAffUrl(affUrl,merchantDomain);
    }

    private void testOneResolveAffUrl(String affUrl,String expectedDomain){
        try {
            String finalUrl = UrlRelatedHelper.calcFinalAffUrl2(affUrl);
            log.debug("finalUrl="+finalUrl);
            Assert.assertTrue(isGotToCorrectUrl(finalUrl,expectedDomain));
        } catch (AffiliationUrlNotValidException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }

    }

    private boolean isGotToCorrectUrl(String finalUrl, String expectedDomain) {
        return finalUrl.startsWith("http://"+expectedDomain) ||
                finalUrl.startsWith("https://"+expectedDomain) ||
                finalUrl.startsWith("http://shop."+expectedDomain) ||
                finalUrl.startsWith("https://shop."+expectedDomain) ;

    }

}
