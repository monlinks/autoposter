package com;

import com.monlinks.dealsposter.dealscollectorservice.DealsCollectorJobExecutor;
import com.monlinks.dealsposter.utils.DateTimeHelper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by lior on 09/05/2017.
 */
public class CronSchedulerTest {
    private static Logger log = LogManager.getLogger(CronSchedulerTest.class.getName());
    private String collectorScheduleCronStr = "0 0 0/2 * * ?";

    @Test
    @Ignore
    public void testCron() throws ParseException, SchedulerException {

        log.info("collectorScheduleCronStr=" + collectorScheduleCronStr);
        CronExpression exp = new CronExpression(collectorScheduleCronStr);
        Date nextRun = exp.getNextValidTimeAfter(new Date());
        log.info(" nextRun=" + nextRun);
        log.info(" nextRun ISRAEL time=" + DateTimeHelper.calcIsraelTime(nextRun));

        //Trigger collectSchedual = buildTrigger("collectSchedual", collectorScheduleCronStr);
        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(collectorScheduleCronStr);

        Trigger trigger = TriggerBuilder.newTrigger().withIdentity("testTriger", "crongroup1").startNow()
                .withSchedule(cronScheduleBuilder).build();
        SchedulerFactory schfa = new StdSchedulerFactory();
        Scheduler sch = schfa.getScheduler();

        //JobDetail jobdetail1 = buildJobDetails2(collectors);
        JobDetail jobdetail = JobBuilder.newJob(DealsCollectorJobExecutor.class).build();
//        jobdetail.getJobDataMap().put("collectors", collectors);
//        jobdetail.getJobDataMap().put("dealsRepository", dealsRepository);
//        jobdetail.getJobDataMap().put("collectorsRepository", collectorsRepository);

        sch.scheduleJob(jobdetail, trigger);

        sch.start();

        while (true);
    }
}
